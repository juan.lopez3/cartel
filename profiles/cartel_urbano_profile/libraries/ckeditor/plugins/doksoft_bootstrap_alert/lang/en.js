﻿/*
Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.md or http://ckeditor.com/license
*/
CKEDITOR.plugins.setLang( 'doksoft_bootstrap_alert', 'en', {

    doksoft_templates_title: 'Insert template',
    doksoft_bootstrap_templates_title: 'Insert Bootstrap template',
    doksoft_alert_title: 'Insert alert',
    doksoft_bootstrap_alert_title: 'Insert Bootstrap alert',
    doksoft_foundation_alert_title: 'Insert Foundation alert',
    doksoft_foundation_templates_title: 'Insert Foundation template',
    doksoft_special_symbols_title: 'Insert special symbol',
    doksoft_smile_title: 'Insert smile',
    select_element_first: 'Select element first'

} );
