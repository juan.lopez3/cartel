<?php

/**
 * @file
 * Disqus as field
 */

/**
 * Implements hook_field_info().
 */
function disqus_field_info() {

  return array(
    'disqus_comment' => array(
      'label' => t('Disqus comment'),
      'description' => t('Disqus comment widget'),
      'settings' => array(),
      'instance_settings' => array(),
      'default_widget' => 'disqus_comment',
      'default_formatter' => 'disqus_comment',
    ),
  );
}

/**
 * Implements hook_field_load().
 */
function disqus_field_load($entity_type, $entities, $field, $instances, $langcode, &$items) {
  $domain = variable_get('disqus_domain', '');
  if (!empty($domain)) {
    $entity_info = entity_get_info($entity_type);
    foreach ($entities as $entity_id => $entity) {
      $uri = $entity_info['uri callback']($entity);

      # Core not yet provide entity title-callback.
      $title = '';
      if (!empty($entity->title)) {
        $title = $entity->title;
      }
      elseif (!empty($entity->name)) {
        $title = $entity->name;
      }

      foreach ($items[$entity_id] as $delta => $item) {
        $items[$entity_id][$delta] = array(
          'domain' => $domain,
          'status' => $item['value'],
          'url' => url($uri['path'], array('absolute' => true)),
          'title' => check_plain($title),
          'identifier' => "{$entity_type}/{$entity_id}",
          'developer' => variable_get('disqus_developer', FALSE),
        );
      }
    }
  }
}

function disqus_field_update($entity_type, $entity, $field, $instance, $langcode, &$items) {
  foreach ($items as $i => $item) {
    if (empty($item['comment_settings'])) {
      continue;
    }
    $items[$i] = array('value' => $item['comment_settings']['disqus_status']);
  }
}

/**
 * Implements hook_field_is_empty().
 */
function disqus_field_is_empty($item, $field) {
  return FALSE;
}

/**
 * Implements hook_field_widget_info().
 */
function disqus_field_widget_info() {

  return array(
    'disqus_comment' => array(
      'label' => t('Default'),
      'field types' => array('disqus_comment'),
      'settings' => array(),
    ),
  );
}


/**
 * Implements hook_field_widget_form().
 */
function disqus_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  switch ($instance['widget']['type']) {
    case 'disqus_comment':
      if (!isset($element['comment_settings'])) {
        $element['comment_settings'] = array(
          '#type' => 'fieldset',
          '#access' => user_access('toggle disqus comments'),
          '#title' => t('Comment settings'),
          '#collapsible' => TRUE,
          '#collapsed' => TRUE,
          '#group' => 'additional_settings',
          '#weight' => 30,
        );
      }
      $element['comment_settings']['disqus_status'] = array(
        '#type' => 'checkbox',
        '#title' => t('Disqus comments'),
        '#description' => t('Users can post comments using <a href="@disqus">Disqus</a>.', array('@disqus' => 'http://disqus.com')),
        // @TODO: Check default value
        '#default_value' => isset($items[$delta]['status']) ? $items[$delta]['status'] : TRUE,
        '#access' => user_access('toggle disqus comments'),
      );
      return $element;
  }
}

/**
 * Implements hook_field_formatter_info().
 */
function disqus_field_formatter_info() {

  return array(
    'disqus_comment' => array(
      'label' => t('Default'),
      'field types' => array('disqus_comment'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function disqus_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {

  $element = array();
  switch ($display['type']) {
    case 'disqus_comment':
      foreach ($items as $delta => $item) {
        $element[$delta] = array(
          '#type' => 'disqus',
          '#disqus' => $item,
        );
      }
      break;
  }
  
  return $element;
}