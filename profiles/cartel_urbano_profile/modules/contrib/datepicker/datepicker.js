(function ($) {

Drupal.behaviors.datepicker = {
  attach: function() {
    for (var id in Drupal.settings.datePopup) {
      $('#'+ id).once(function() {
        datePopup = Drupal.settings.datePopup[id];
        datePopup.settings.onSelect = function(date) {
          $('#' + id).trigger('keyup');
        };
        switch (datePopup.func) {
          case 'datepicker-inline':
            $(this).wrap('<div id="' + id + '-wrapper" />');
            if (datePopup.settings.defaultDate != null) {
              datePopup.settings.defaultDate = $('#' + id).val();
            }
            $(this).parent().datepicker(datePopup.settings);
            $(this).hide();

            Drupal.ajax.prototype.commands.settings = function (ajax, response, status) {
              ajax.settings = response.settings;
            };
          break;
        }
      });
    }
  }
 }

})(jQuery);
