(function($) {
//Drupal.behaviors.cu_mas_popular = {
  //attach: function (context, settings) {
    //code starts
    $(document).ready(function(){
        $.ajax({
            url: Drupal.settings.basePath + '?q=eventos_populares&items=eventos',
            type: 'post',
            dataType: 'json',
            success: function (data) {
                if (data.success === true) {
                    $('#block-cu-mas-popular-los-eventos-mas-popular').find('img.img-temp').hide(300);
                    $('.field-name-eventos-horizontales').find('img.img-temp').hide(300);
                    $('#container-popular-events').html(data.content);
                    $('#container-popular-events').removeClass('carga-imagen');
                    $('#container-popular-events').addClass('carga-nodos');
                } else {
                    $('#container-popular-events').addClass('carga-imagen');
                    $('#container-popular-events').removeClass('carga-nodos');
                }
            },
        });

        $.ajax({
            url: Drupal.settings.basePath + '?q=eventos_populares&items=paginas',
            type: 'post',
            dataType: 'json',
            success: function (data) {
                if (data.success === true) {
                    $('.field-name-lo-mas-popular-block').find('img.img-temp').hide(300);
                    $('#block-cu-mas-popular-las-secciones-mas-populares').find('img.img-temp').hide(300);
                    $('#container-popular-paginas').html(data.content);
                    $('#container-popular-events').removeClass('carga-imagen');
                    $('#container-popular-events').addClass('carga-nodos');
                } else {
                    $('#container-popular-events').addClass('carga-imagen');
                    $('#container-popular-events').removeClass('carga-nodos');
                }
            },
        });
    });
    //code ends
  //}
//};
})(jQuery);