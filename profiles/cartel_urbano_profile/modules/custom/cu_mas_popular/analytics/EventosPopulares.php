<?php

/**
 * @file Cunsultas a google analyics para traer datos y guardarlos en cache de drupal
 */


require 'AnalyticsApiConnector.php';

/**
 * Class EventosPopulares
 *
 */
class EventosPopulares {


	public $cnn;
	public $id;

	/**
	 * EventosPopulares constructor.
	 */
	public function __construct() {
		//Instancia de la conexion GA
		$this->cnn = new AnalyticsApiCnn();
		//Id obtenido de GA $this->cnn->getFirstprofileId();
		$this->id = 122346213;
	}

	/**
	 * @param  {String} $type
	 * @method Rertorna el listado de artículos populares
	 * @return {Array} nodes
	 */
	function paginasPopulares() {
		//Query
		$type = 'paginas';
		//LLama los datos de cache
		$data = $this->getFromCache($type);

		//Si hay datos los muestra
		if ($data !== NULL || $data != null) {
			//	return $data;
		}

		//Consigue las url más populares de GA
		$urls = $this->executeQuery(array($this, "getTopPages"));
		//Carga los nodos correspondientes a ls urls consultadas
		$nodes = $this->loadNodes($urls);
		//Renderiza los nodos
		$output = $this->renderNodes($nodes, 'articulos_recomendados');
		//Guarda en cache los nodos consultados
		$this->putInCache($output, $type);

		return $output;
	}

	/**
	 * @param  {String} $type
	 * @method Rertorna el listado de eventos o artículos populares
	 * @return {Array} nodes
	 */
	public function eventosPopulares() {

		//Query
		$type = 'eventos';
		//LLama los datos de cache
		$data = $this->getFromCache($type);

		//Si hay datos los muestra
		if ($data !== NULL || $data != null) {
			return $data;
		}

		//Consigue las url más populares de GA
		$urls = $this->executeQuery(array($this, "getTopEvents"));
		//Carga los nodos correspondientes a ls urls consultadas
		$nodes = $this->loadNodes($urls);
		//Verifica que los eventos tengan fechas validas
		$nodes = $this->filtroPorFecha($nodes);
		//Verifica que tenga concurso de boletas
		$nodes = $this->validarCampoEntradas($nodes);
		//Renderiza los nodos
		$output = $this->renderNodes($nodes, 'eventos_recomendados');

		if (empty($output)) {
			$nodes = $this->get_lastest_events();
			$output = $this->renderNodes($nodes, 'eventos_recomendados');
		}

		//Guarda en cache los nodos consultados
		$this->putInCache($output, $type);
		return $output;
	}

	/**
	 * @method Consigue las url más populares de GA de tipo evento
	 * @param $analytics
	 * @param $profileId
	 * @return {Array}
	 */
	public function getTopEvents(&$analytics, $profileId) {

		$date = new DateTime();
		$today = $date->format('Y-m-d');
		$date->add(DateInterval::createFromDateString('yesterday'));
		$yesterday = date('Y-m-d', strtotime('-4 week'));

		$optParams = array(
			'max-results' => 20,
			'dimensions' => 'ga:pageTitle,ga:pagePath',
			'sort' => '-ga:pageviews',
			'filters' => 'ga:pagePath=~^/eventos/*;ga:pagePath!~^/seccion/*/;ga:pagePath!=/eventos/agenda',
		);

		$resultados = null;
		try {
			/* Se realiza la consulta con las dimensiones/metricas/filtrso requeridos */
			$resultados = $analytics->data_ga->get(
				'ga:' . $profileId,
				$yesterday,
				$today,
				'ga:pageviews',
				$optParams);
		} catch (Exception $e) {
			//Escribir el mensaje de la Exception en el log de drupal
			watchdog('cu_mas_popular', $e->getMessage(), array(), WATCHDOG_ERROR);

		}

		return $resultados;
	}

	/**
	 * @method Consigue las url más populares de GA de tipo artículo
	 * @param $analytics
	 * @param $profileId
	 * @return {Array}
	 */
	public function getTopPages(&$analytics, $profileId) {

		$date = new DateTime();
		$today = $date->format('Y-m-d');
		$date->add(DateInterval::createFromDateString('yesterday'));
		$yesterday = date('Y-m-d', strtotime('-4 week'));

		$optParams = array(
			'max-results' => 20,
			'dimensions' => 'ga:pageTitle,ga:pagePath',
			'sort' => '-ga:pageviews',
			//'filters' => 'ga:pagePath!~^/eventos/*',
			'filters' => 'ga:pagePath!~^/seccion/*/',

		);
		$resultados = null;
		try {
			/* Se realiza la consulta con las dimensiones/metricas/filtrso requeridos */
			$resultados = $analytics->data_ga->get(
				'ga:' . $profileId,
				$yesterday,
				$today,
				'ga:pageviews',
				$optParams);

		} catch (Exception $e) {
			watchdog('cu_mas_popular', $e->getMessage(), array(), WATCHDOG_ERROR);
		}
		return $resultados;
	}


	/**
	 * @method Realiza la consulta a analytics y verifica las url de los resultados
	 * @param {String} $type
	 */
	public function executeQuery($queryFunction) {

		$analytics = $this->cnn->getService();
		$profile = $this->cnn->getFirstProfileId($analytics);

		$results = $queryFunction($analytics, $this->id);
		$items_populares = $results["rows"]; //Se separan las url y titulos de las paginas
		$items = $this->filtrarUrlRotas($items_populares);

		return $items;

	}


	/**
	 * @method Valida que los eventos no hallan terminado aún
	 * @param  {Array} $nodes
	 * @return {Array} $nodes
	 */
	public function filtroPorFecha($nodes) {
		$fecha_actual = date('Y-m-d H:i:s');
		foreach ($nodes as $key => $node) {
			$fecha_evento = isset($node->field_fecha['und'][0]['value2']) ? $node->field_fecha['und'][0]['value2'] : NULL;
			if (!$fecha_evento || $fecha_evento < $fecha_actual) {
				unset($nodes[$key]);
			}

		}
		return $nodes;
	}

	/**
	 * @method Elimina el campo gane entradas de los eventos sin formulario
	 * @param  {Array} $nodes
	 * @return {Array} $nodes
	 */
	public function validarCampoEntradas($nodes) {
		foreach ($nodes as $key => $node) {
			if (!isset($node->webform)) {
				$node->gane_entradas = '';
			}
		}
		return $nodes;
	}

	private function loadNodes($urls) {
		$items = array();

		foreach ($urls as $key => $url) {
			$node_ruta = ltrim($url[1], '/');
			$path = drupal_lookup_path("source", $node_ruta);
			$nodo = menu_get_object("node", 1, $path);

			if (!$nodo) continue;


			if (isset($nodo->type) && ($nodo->type == 'eventos' || $nodo->type == 'article')) {
				$items[] = $nodo;
			}
		}


		return $items;
	}

	/**
	 * @methos Consigue los nodos y retorna un listado de nodos con display suite
	 * @param  {Array} $nodes
	 * @param  {String} $type
	 * @return {Array} $items
	 */
	public function renderNodes($nodes, $display) {

		$items = array();
		foreach ($nodes as $key => $node) {
			$node = node_view($node, $display);
			$items[] = $node;
		}
		return array_slice($items, 0, 5);
	}

	/**
	 * @method Guardar en variables drupal el resultado de la consulta de analytics
	 * @param  {Array} $nodes
	 * @return {Array} $nodes
	 */
	public function putInCache($nodes, $type) {
		variable_del($type . '_incache');
		$fecha_actual = date('Y-m-d H:i:s');
		//Crea array a guardar
		$data = array(
			'fecha' => $fecha_actual,
			'data' => $nodes
		);
		variable_set($type . '_incache', $data);
	}

	/**
	 * @param {String} $type
	 * @return {Array} Content of variable
	 */
	public function getFromCache($type) {

		$data = variable_get($type . '_incache', $default = NULL);
		return $data['data'];

		if (!isset($data['fecha']) || !empty($data['fecha']) || $data['fecha'] == '') return NULL;
		$fecha_guardada = date($data['fecha']);
		$ahora = date('Y-m-d H:i:s');
		$days = $this->countDaysBetween($fecha_guardada, $ahora);


		if ($days < 2 && $days > 8 && (isset($data['data']) && !empty($data['data']))) {
			return $data['data'];
		}
		return NULL;
	}


	/**
	 * @method Valida que las url sean válidas
	 * @param $ruta
	 */
	private function validar_url_rotas($ruta) {
		if (strpos($ruta, '404') || $ruta == '/' || strpos($ruta, '/content/')) {
			return;
		}
		return $ruta;
	}

	/**
	 * @method Valida los articulos por nombre
	 * @param $todos
	 * @return array
	 */
	private function filtrarUrlRotas($todos) {
		$items = array();
		foreach ($todos as $key => $value) {

			$article_name = explode('|', $value[0]);
			$article_name = $article_name[0];
			$ruta = $this->validar_url_rotas($value[1]);
			//verificamos que la URL devuelta por google existe
			if ($ruta == '' || strpos($article_name, 'wut?') && $article_name != '') {
				continue;
			}
			$items[] = $value;
		}

		return $items;
	}

	/**
	 * @method Count the days between two dates given
	 * @param $from
	 * @param $to
	 * @return false|string
	 */
	private function countDaysBetween($from, $to) {
		$first_date = strtotime($from);
		$second_date = strtotime($to);
		return date('d', $first_date) - date('d', $second_date);
	}

	private function get_lastest_events() {
		// Return all nids of nodes of type "page".
		$nids = db_select('node', 'n')
			->fields('n', array('nid'))
			->condition('n.type', 'eventos')
			->range(0, 5)
			->orderBy('n.created', 'ASC')
			->execute()
			->fetchCol(); // returns an indexed array
		// Now return the node objects.
		$nodes = node_load_multiple($nids);
		return $nodes;
	}

}
