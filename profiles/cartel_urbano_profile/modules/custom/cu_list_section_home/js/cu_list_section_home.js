

(function($) {
    $(document).ready(function() {

        // $('.ajax_link_abrir').css({'display': 'none'});
        var id = 0;

        $('.container_section').attr('data-active', false).on('click', function() {

            var active = $(this).attr('data-active');
            if (active === 'true')
                return;

            if (id == $(this).attr('id'))
                return;
            id = $(this).attr('id');

            $('#ajax_link_abrir_' + id).click();
            $('.container_section').attr('data-active', false);
            $(this).attr('data-active', true);


        });
    });

})(jQuery);


(function($) {
    $.fn.grid_content_loading = function($id) {

        //limpiamos las clases del contenedor
        $('.container_section').removeClass('cargando, active');
        //agregamos clase cargando para poner estilos al loader
        var secWrap = $('div#tid_' + $id);
        secWrap.addClass('cargando');
    };

    $.fn.grid_content_loaded = function($id) {
        
        //selector padre
        var secWrap = $('div#tid_' + $id);
        //quitamos clase cargando
        //agrega clase activa el contenedor actual
        secWrap.delay(1000).queue(function(next){
            $(this).addClass('active');
            $('.container_section').removeClass('cargando');
            next();
        });
        //cambia label 'autor'    
        $(".field-name-author.field-type-ds > div.field-label").html("Por: ");

        console.log('testing loaded' + $id);
    };
})(jQuery);

