//cu_galeria_modal
(function($) {

  $(document).ready(function() {
    var count = 0;

    var slider = $(".slick__slider.slick-initialized.slick-slider");

    slider.find(".slick-track .slick__slide.slide--caption--right.slick-slide .slide__content").each(function() {
      count++;
      $(this).attr('custom-state-slider', count);

    });


    var foo = slider.find('.slick-track .slide__media');

    // On before slide change
    $('.slick__slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){

        var nSlide = (nextSlide == currentSlide.slideCount) ? 0 : nextSlide + 1;

        $(foo[nSlide]).addClass('oculto');
    });


    $('.slick__slider').on('lazyLoaded', function (slick, currentSlide) {


      var cSlide = (currentSlide.currentSlide == currentSlide.slideCount) ? 0 : currentSlide.currentSlide + 1;
      var SlideMedia = $('.slide__media'),
          hSlideMedia = SlideMedia.height(),
          wSlideMedia = SlideMedia.width();

        var _img = $(foo[cSlide]).find('.media img');
        var _w, _h;

        _w = _img.width();
        _h = _img.height();

        console.log('hSlideMedia, wSlideMedia, _w, _h', hSlideMedia, wSlideMedia, _w, _h, _img);


      //imagenes de la galeria
      function typeImage (w, h, img) {
        var type = (w < h) ? 'portrait' : 'landscape';
        var _w, _h;


        $(foo[cSlide]).addClass(type);

        _w = img.width();
        _h = img.height();
        
        if (wSlideMedia > _w || hSlideMedia > _h) {
          if (type === 'portrait') {
            //align image axis x
            var moveImage = (wSlideMedia - _w) / 2;
            $(img).css({'transform': 'translateX('+ moveImage +'px)'})
            console.log('move axis X', moveImage);
            return;
          }

          if (type === 'landscape') {
            //align image axis y
            var moveImageY = (hSlideMedia - _h) / 2;
            $(img).css({'transform': 'translateY('+ moveImageY +'px)'})
            console.log('move axis Y', moveImageY);
            return;
          }
        }
      }

      typeImage(_w, _h, _img);
      $(foo[cSlide]).removeClass('oculto');
    });


    var actual = 1;

    var paginador = '<div id="pager">' +
      '<div id="next">&lt;</div>' +
      '<div class="content-pages">' +
      '<div class="wrap">' +
      '<div class="pag-actual" id="actual-slide"> ' + actual + '</div>' +
      '<span>/</span>' +
      '<div class="pag-total">' + (count - 2) + '</div>' +
      '</div>' +
      '</div>' +
      '<div id="prev">&gt;</div>' +
      '</div>';

    var $banner = $(".field-name-publicidad").find("script").text();
    var $id_banner = ($banner) ? $banner.split("(")[1] : "";
    $id_banner = $id_banner.substring(0, $id_banner.length - 2);

    slider.on('afterChange', function(event, slick, currentSlide) {
      $("#actual-slide").text(currentSlide + 1);
      var activo = $(" .slick.slick--skin--classic.slick--optionset--galerias.slick--display--main").find(".slick-slide.slick-active");
      var img_active = activo.find("img").attr("src");
      var url_push = window.location.hash = '/sites/' + img_active.split("sites/")[1];

      /*
                          Send custom event to tag manager analytics
      */


      /*
        Galerías page view everytime slider is clicked
      */
      dataLayer.push({
        'event': 'gallery_clicked',
        'virtualPageURL': url_push,
        'virtualPageTitle': 'gallery' + url_push
      });
      	console.log("Enviando evento ....");
      ga('send', {
  			hitType: 'pageview',
  			page: url_push
		});

      //$(".field-name-publicidad").html('<script>broadstreet.zone(49783);</script>');
      var $objetivo = $(".field-name-publicidad .field-items .field-item .publicidad div");
      $objetivo.empty(); //;
      var publicidad = '<iframe scrolling="no" frameborder="0" src="http://ad.broadstreetads.com/zdisplay/' + $id_banner + '.html"></iframe>';
      $(publicidad).appendTo($objetivo);


    });
    $(".slick-wrapper.slick-wrapper--asnavfor.slick-wrapper--classic").prepend(paginador);


    $('#next').click(function() {
      slider.slick('slickPrev');
    });

    $('#prev').click(function() {
      slider.slick('slickNext');
    });

    //move first ad banner inside the slider container
    var pub = $(".field.field-name-publicidad.field-type-ds.field-label-hidden");
    //pub.remove();
    //$(".slick-wrapper.slick-wrapper--asnavfor.slick-wrapper--classic .slick--optionset--galerias.slick--display--main .slide__caption").prepend (pub);

    if (parseInt($(window).width()) > 768) {
      $(".slick-wrapper.slick-wrapper--asnavfor.slick-wrapper--classic .slick--optionset--galerias.slick--display--main ").prepend(pub);
    }
  });




})(jQuery);
