
(function($) {
   $(document).ready(function() {


      $(".view.view-ver-mas-home.view-id-ver_mas_home div.hidden").hide();


      $('span#ajax_link').click(function(e) {

         $(".view.view-ver-mas-home.view-id-ver_mas_home div.hidden").show();

         $(".view.view-ver-mas-home.view-id-ver_mas_home div.hidden").removeClass("hidden");
         estilo_db_columna();
         masonry_ajax();
         $('span#ajax_link').hide();

      });


      //loader ver mas
      $('body').addClass('eventos-cargados');
      $(document).bind("ajaxStart", function() {
         //cuando esta cargando los nodos
         console.log('ajax start');
         $('body').removeClass('eventos-cargados').addClass('cargando-eventos');
      }).bind("ajaxComplete", function() {
         //cuando carga los nodos
         estilo_db_columna();
         masonry_ajax();

         $('body').removeClass('cargando-eventos').addClass('eventos-cargados');

      });
      /*recarga la lista de nodos al cerrar/abrir el menu */
      $(".abrir-menu.icon-cartel-ico-menu").click(function(){
         console.log("clicked menu");
         estilo_db_columna();
         masonry_ajax();
      });


   });

   function estilo_db_columna() {
      var item = $('.pane-ver-mas-home').find('ul').find('.view-mode-card_destacado_home');
      var itemNormal = $('.pane-ver-mas-home').find('ul').find('.view-mode-destacados_home_general');

      //agregar selector en contendor principal del nodo
      item.each(function(){
         var parent = $(this).closest('li.views-row');
         parent.addClass('nodo-destacado-home');
      });

      //selector para definir 'columWidth' de masonry
      itemNormal.closest('li.views-row').addClass('columnWidth');
   }

   function masonry_ajax(){
      var container = $('.pane-ver-mas-home').find('ul');

      // layout Isotope after each image loads
      container.imagesLoaded().done( function(response) {
         var grid = container.masonry({
            itemSelector: '.views-row',
            percentPosition: true,
            columnWidth: '.views-row.columnWidth'
         });
         grid.masonry('reloadItems');
         grid.masonry('layout');

      });
   }

})(jQuery);
