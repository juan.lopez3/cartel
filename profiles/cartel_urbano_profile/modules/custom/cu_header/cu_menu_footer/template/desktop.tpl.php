
<div class="container_footer footer-desktop">

	<div class="contenedor-superior">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12">
					<div class="newslleter_footer">
						<?php print $newslleter_form; ?>
					</div>

					<div class="politica-datos">
						<div class="text">
							<a href="<?php print base_path() . 'proteccion-de-datos'; ?>"> <img src="<?php global $t; print $t?>/assets/img/footer/politica-de-proteccion-de-datos.svg" ></a>
						</div>
					</div>
					<img src="<?php global $t; print $t?>/assets/img/footer/Flanja.svg" >
				</div>
			</div>
		</div>
	</div>

	<div class="footer-info">
		<div class="container-fluid">
			<div class="row">

				<div class="col-footer-info col-sm-4">
					<div class="container_1">
						<div class="enlaces_internos">
							<div class="contentTitulo">
								<h3>SECCIONES</h3>
							</div>
							<div class="list_sections">
								<?php print $list_sections; ?>
							</div>
							<div class="container_link_general footer">
								<li><a href="<?php global $p; print $p?>eventos/agenda" >AGENDA</a></li>
								<li><a href="<?php global $p; print $p?>carteltv" >Cartel TV</a></li>
								<li><a href="<?php global $p; print $p?>galerias">Galerías</a></li>
								<li><a href="<?php print base_path().'ediciones-impresas'; ?>">Ed. Impresa</a></li>
							</div>
						</div>
					</div>
				</div>

				<div class="col-footer-info col-sm-4">
					<div class="container_2">
						<div class="redes-sociales">
							<div class="contentTitulo">
								<h3>CONTÁCTENOS</h3>
							</div>
							<ul class="redes_footer">
								<li class="facebook"><a target="_blank" href="https://www.facebook.com/revistacartelurbano" ><img src="<?php global $t; print $t?>/assets/img/footer/facebook.png" ></a></li>
								<li class="twitter"><a target="_blank" href="https://twitter.com/cartelurbano" ><img src="<?php global $t; print $t?>/assets/img/footer/twitter.png" ></a></li>
								<li class="instagram"><a target="_blank" href="http://instagram.com/cartelurbano" ><img src="<?php global $t; print $t?>/assets/img/footer/instagram.png" ></a></li>
								<li class="youtube"><a target="_blank" href="mailto:info@cartelurbano.com" ><img src="<?php global $t; print $t?>/assets/img/footer/email.png" ></a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-footer-info col-sm-4">
					<div class="container_3">
						<div class="enlaces_externos">
							<div class="wrap_nuestros_amigos">
								<h3>NUESTROS AMIGOS</h3>
								<div class="container-nuestros_amigos">
									<ul class="nuestros_amigos">
										<li class="bacanika">
											<a target="_blank" href="http://www.bacanika.com/">BACANIKA</a></li>
										<li class="revistametronomo">
											<a target="_blank" href="http://www.revistametronomo.com/">REVISTA METRÓNOMO</a></li>
										<li class="furiamag">
											<a target="_blank" href="http://www.furiamag.com/">FURIAMAG</a></li>
										<li class="zona57">
											<a target="_blank" href="http://www.zona57.com/">ZONA 57</a></li>
										<li class="rockombia">
											<a target="_blank" href="https://www.rockombia.com/">ROCKOMBIA</a></li>
										<li class="copublicitarios">
											<a target="_blank" href="http://copublicitarias.com/">CO PUBLICITARIAS</a></li>
										<li class="pxtv">
											<a target="_blank" href="http://www.pxtv.tv/">PXTV</a></li>
										<li class="topnube">
											<a target="_blank" href="http://www.topnube.com/">TOP NUBE</a></li>
										<li class="creartika">
											<a target="_blank" href="http://creartika.com/">CREARTIKA</a></li>
									</ul>
									<ul class="nuestros_amigos">
										<li class="creartika">
											<a target="_blank" href="http://www.mincultura.gov.co/Paginas/default.aspx">MINISTERIO DE CULTURA</a></li>
										<li class="creartika">
											<a target="_blank" href="http://www.emprendimientocultural.org/Paginas/default.aspx">EMPRENDIMIENTO CULTURAL</a></li>
										<li class="creartika">
											<a target="_blank" href="http://creartika.com/">CARTEL URBANO EN DEEZER</a></li>
										<li class="creartika">
											<a target="_blank" href="http://www.ccb.org.co/">CÁMARA DE COMERCIO DE BOGOTÁ</a></li>
										<li class="creartika">
											<a target="_blank" href="http://guayabocolectivo.com/">GUAYABO COLECTIVO</a></li>
										<li class="creartika">
											<a target="_blank" href="http://mocionsoft.com/">MOCION SOFTWARE</a></li>
										<li class="creartika">
											<a target="_blank" href="http://www.uncuartotech.com/">1/4 TECH</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="footer-middle">
		<!-- Cartel image -->
		<img src="<?php global $t; print $t?>/assets/img/footer/FlanjaCartelMedia.svg" >
	</div>

	<div class="footer-bottom">
		<div class="container-fluid">

			<div class="row">
				<div class="col-sm-12">
					<div class="sobre_cartel">
						<!--<span>SOBRE CARTEL URBANO</span>-->
						<p>Somos una plataforma de difusi&oacute;n de la movida alternativa en Colombia y Latinoam&eacute;rica. <br />Le hablamos a una generaci&oacute;n de j&oacute;venes que se expresan a trav&eacute;s de las manifestaciones de la cultura emergente urbana.</p>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-4 col-footer-bottom">
					<div class="list-icons">
						<ul>
							<li class="nodo-icon-li"><div class="icon nodo-icon"><img src="<?php global $t; print $t?>/assets/img/footer/Logo-Nodo.svg" ></div></li>
							<li class="creadores-icon-li"><div class="icon creadores-icon"><img src="<?php print $t?>/assets/img/footer/Shock.svg" ></div></li>
						</ul>
					</div>
					<div id="direccion" class="info_site">Carrera 22 # 75 - 18 / Tel: 316 7431788. Bogotá, Colombia</div>
				</div>
				<div class="col-sm-4 col-footer-bottom">
					<div class="list-icons">
						<ul>
							<li><div class="icon creadores-icon"><img src="<?php print $t?>/assets/img/footer/Logo-CreadoreCriollos.svg" ></div></li>
							<li><div class="icon youth-icon"><img src="<?php print $t?>/assets/img/footer/Logo-YouthScan.svg" ></div></li>
							<li><div class="icon antidoto-icon"><img src="<?php print $t?>/assets/img/footer/Logo-ElAntidoto.svg" ></div></li>
							<br>
							<li class="cartel-icon-li"><div class="icon cartel-icon"><img src="<?php print $t?>/assets/img/footer/Logo-CartelMedia.svg" ></div></li>
						</ul>
					</div>
				</div>

		<!--		<div class="col-sm-4 col-footer-bottom">
				
					<div class="col-sm-6 icon iab-icon"><a href="" target="_blank"><img src="<?php print $t?>/assets/img/footer/logoiab.svg"></a></div>
					<div class="col-sm-6 icon mocion-icon"> <a target="_blank" href="www.mocionsoft.com/?utm_source=cartel&utm_medium=pagina%20web%20aliado&utm_campaign=aliados
"><img src="<?php print $t?>/assets/img/footer/Logo-PoweredByMocion.svg" ></a></div>

					<div id="derechos_autor" class="info_site">Todos los derechos reservados, Cartel Media S.A.S © <?php print date("Y"); ?></div>
				</div>-->


				<div class="col-sm-4 col-footer-bottom">
					<div class="list-icons">
						<ul>
							<li class="iab-icon-li"><div class="icon iab-icon"><img src="<?php global $t; print $t?>/assets/img/footer/logoiab.svg" ></div></li>
							<!--<li class="mocion-icon-li"><div class="icon mocion-icon"><a target="_blank" href="www.mocionsoft.com/?utm_source=cartel&utm_medium=pagina%20web%20aliado&utm_campaign=aliados"><img src="<?php print $t?>/assets/img/footer/Logo-PoweredByMocion.svg"></a></div></li>-->
						</ul>
					</div>
					<div id="derechos_autor" class="info_site">Todos los derechos reservados, Cartel Media S.A.S © <?php print date("Y"); ?></div>
				</div>				
			</div>
		</div>
	</div>

</div>
