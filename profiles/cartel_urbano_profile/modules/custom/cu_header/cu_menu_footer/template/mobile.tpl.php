
<div class="container_footer footermobile">

    <div class="contenedor-superior">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="newslleter_footer">
                        <?php print $newslleter_form; ?>
                    </div>

                    <div class="politica-datos">
                        <div class="text">
                            <a href="<?php print base_path() . 'proteccion-de-datos'; ?>"> <img src="<?php global $t; print $t?>/assets/img/footer/politica-de-proteccion-de-datos.svg" ></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-info">
        <div class="container-fluid">
            <div class="row">

                <div class="col-xs-12">
                    <div class="container_2">
                        <div class="redes-sociales">
                            <h3>CONTÁCTENOS</h3>
                            <ul class="redes_footer">
                                <li class="facebook"><a target="_blank" href="https://www.facebook.com/revistacartelurbano" ><img src="<?php global $t; print $t?>/assets/img/footer/facebook.png" ></a></li>
                                <li class="twitter"><a target="_blank" href="https://twitter.com/cartelurbano" ><img src="<?php global $t; print $t?>/assets/img/footer/twitter.png" ></a></li>
                                <li class="instagram"><a target="_blank" href="http://instagram.com/cartelurbano" ><img src="<?php global $t; print $t?>/assets/img/footer/instagram.png" ></a></li>
                                <li class="youtube"><a target="_blank" href="mailto:info@cartelurbano.com" ><img src="<?php global $t; print $t?>/assets/img/footer/email.png" ></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="footer-middle">
        <!-- Cartel image -->
        <img src="<?php global $t; print $t?>/assets/img/footer/Franja-CartelUrbanoMovil.svg" >
    </div>

    <div class="footer-bottom">
        <div class="container-fluid">


            <div class="row">
                <div class="col-xs-12 col-footer-bottom">
                    <div class="list-icons">
                        <ul class="list-icons-uno">
                            <li><div class="icon nodo-icon"><img src="<?php global $t; print $t?>/assets/img/footer/Logo-Nodo.svg" ></div></li>
							<li><div class="icon creadores-icon"><img src="<?php print $t?>/assets/img/footer/Shock.svg" ></div></li>

                            <br>

                            <li><div class="icon creadores-icon"><img src="<?php print $t?>/assets/img/footer/Logo-CreadoreCriollos.svg" ></div></li>
                            <li><div class="icon youth-icon"><img src="<?php print $t?>/assets/img/footer/Logo-YouthScan.svg" ></div></li>
                            <li><div class="icon antidoto-icon"><img src="<?php print $t?>/assets/img/footer/Logo-ElAntidoto.svg" ></div></li>
                        </ul>

                        <ul class="list-icons-dos">
                            <li><div class="icon cartel-icon"><img src="<?php print $t?>/assets/img/footer/Logo-CartelMedia.svg" ></div></li>
                            <!--<li>  <div class="icon mocion-icon"><img src="<?php print $t?>/assets/img/footer/Logo-PoweredByMocion.svg" ></div></li>-->
                        </ul>

                        <div class="col-footer-text">
                            <div id="direccion" class="info_site">Carrera 22 # 75 - 18 / Tel: 316 7431788. Bogotá, Colombia</div>
                            <div id="derechos_autor" class="info_site">Todos los derechos reservados, Cartel Media S.A.S © <?php print date("Y"); ?></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
