
(function($) {
    $(document).ready(function() {

        //$('.ajax_link_abrir').css({'display': 'none'});

        var id = 0;
        $('ul#menu-sections li').css({'cursor': 'pointer'}).attr('data-active', false);

        $('#block-cu-menu-sections-block-menu-sections').on('mouseenter', 'ul#menu-sections li', function() {

            var active = $(this).attr('data-active');
            if (active === 'true') {
                //$('div#div_sub_menu_section').show();
                return;
            }

            $('ul#menu-sections li').removeClass('active').attr('data-active', false);


            $(this).attr('data-active', true).addClass('active');

            id = $(this).attr('id');

            var url = Drupal.settings.cu_menu_sections.url_public_files;
            var ultimos_section = url + 'content_sub_menu_nodes_section/' + id + '.html';
            
            //$("div#div_sub_menu_section").html('<img src="' + Drupal.settings.cu_menu_sections.url_public_files_loading +'" style="width: 150px;"/>');

            $.get(ultimos_section, function(data) {

                //$('div#div_sub_menu_section').show();

                $('div#div_sub_menu_section').html(data)
                        .find('#flexslider-items-submenu')
                        .flexslider({
                            animation: 'slide',
                            animationLoop: true,
                            controlNav: false,  
                            slideshow: false,
                        });

            });



        });
        $('#block-cu-menu-sections-block-menu-sections').on('mouseleave', function() {
            //$('div#div_sub_menu_section').hide();
        });

        
        $(".page-node button#edit-submit-agenda").click();
        $(".page-node ul.links.list-inline").hide();

    });
})(jQuery);