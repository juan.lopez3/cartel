//alert('hola');

(function($) {
    $(document).ready(function() {
        
        $('.menu-attach-block-wrapper').css({'display': 'block'});

        $('.block span.form-required').css({'display': 'none'});

        //cambiar action deL formulario de busqueda cada vez que el input cambie
        //se hace ya que por defecto esta tomando un action equivocado y esta saliendo un mensaje de error
		$('form#search-block-form').on('input propertychange paste', function() {
			var palabra = $('form#search-block-form input[type="text"]').val();
		    $("form#search-block-form").attr("action","/search/site/"+palabra);
		});

    });
})(jQuery);