<?php


$plugin = array(
  'title' => t('Cabezote'),
  'category' => t('Cartel Urbano'),
  'render callback' => 'cartel_secciones_related_render',
);

/**
 * Run-time rendering of the body of the block (content type)
 * See ctools_plugin_examples for more advanced info
 */
function cartel_secciones_related_render($subtype, $conf, $args, $contexts) {
 
  $block = new stdClass();
  $block->content = cartel_secciones_related();
  return $block;
}

function cartel_secciones_related($term = FALSE, $tid = 0) {
  $term = $term ? $tid : arg(2);
  $entity = taxonomy_term_load($term);
  $name=$entity->name;
  $icon=$icon=isset($entity->field_icono[LANGUAGE_NONE][0]['value'])?$entity->field_icono[LANGUAGE_NONE][0]['value']:null;
  $imgurl=$entity->field_imagen[LANGUAGE_NONE][0]['uri'];
  $variables = array('style_name' => 'fondoseccion', 'path' => $imgurl, 'width' => NULL, 'height' => NULL, 'alt' => $entity->field_imagen[LANGUAGE_NONE][0]['alt'], 
    'title' => $entity->field_imagen[LANGUAGE_NONE][0]['title'], 'attributes' => NULL);
  $imagen = theme_image_style($variables);
    $render['container'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('cabezote-seccion')),
    );
    $render['container']['background'] = array(
      '#type' => 'markup',
      '#markup' =>$imagen,
      '#attributes' => array('class' => array('imagen-seccion')),
    );

    $render['container']['titulo-seccion'] = array(
      '#type' => 'container' ,
      '#attributes' =>  array(
        'class' => 'titulo-seccion' 
      )
    );

     $render['container']['titulo-seccion']['titulo'] = array(
      '#type' => 'markup',
      '#prefix' => '<h1>',
      '#suffix' => '</h1>', 
      '#markup' =>$name,
      '#attributes' => array('class' => array('titulo-seccion')),
    );

    $render['container']['titulo-seccion']['span'] = array(
      '#type' => 'markup',
      '#markup' => "<span class='cartel-icono icon-cartel-" . $name . "'>".$icon."</span>",
    );

    $render['container']['back'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('background-titulo-seccion')),
    );

    $render['container']['back']['entradilla'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('texto-entradilla-seccion')),
    );
    
     $render['container']['back']['entradilla']['descripcion'] = array(
      '#type' => 'markup',
      '#prefix' => '<p>',
      '#suffix' => '</p>', 
      '#markup' =>$entity->description,
      '#attributes' => array('class' => array('descripcion-seccion')),
    );
   

    return drupal_render($render);

  return FALSE;
}