<?php

$detect = new Mobile_Detect;
$device_type = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
define("DEVICE_TYPE_CARTEL_SECCIONES", 'computer');

$plugin = array(
  'title' => t('Contenido relacionado'),
  'category' => t('Cartel Urbano'),
  'edit form' => 'cartel_secciones_edit_form', // A function that will return the settings form for the pane.

  'render callback' => 'cartel_secciones_header_render',
  );

/**
 * Run-time rendering of the body of the block (content type)
 * See ctools_plugin_examples for more advanced info
 */
function cartel_secciones_header_render($subtype, $conf, $args, $contexts) {

/*Bloque con el flexslider 
  Flexslider con el id del div que lo contiene 'otras secciones' es el nombre de sistema de la configuración de   flexslider aplicada
*/
  flexslider_add('related_content_slider', 'otras_secciones');

  $block = new stdClass();
  $block->title = 'CONTENIDOS RELACIONADOS';
  $block->content = secciones_slider();
  if($block->content != '<div class="cabezote-seccion" id="related_content_slider"></div>'){
    return $block;
  }
}
function cartel_secciones_edit_form($form, &$form_state) {

  return $form;
}
function secciones_ultimos(){

  $node = menu_get_object();
  //kpr($node);
  $avoid = isset($node->vid) ? $node->vid : 1;
  /*
  // Build the content types filter.
  $content_types = array('article', 'gallery', 'video');
  $content_type_filter = new SolrFilterSubQuery('OR');
  foreach ($content_types as $content_type) {
    $content_type_filter->addFilter('bundle', $content_type);
  }
   
  /// Build the Issues filter.
  $issues = array();
  if ($items = field_get_items('node', $node, 'field_issues')) {
    foreach ($items as $issue) {
      $issues[] = 'node:' . $issue['target_id'];
    }
  }
  $issues_filter = new SolrFilterSubQuery('OR');
  foreach ($issues as $issue) {
    $issues_filter->addFilter('sm_field_issues', $issue);
  }
   
  // Build the Topics filter.
  $topics = array();
  if ($items = field_get_items('node', $node, 'field_topics')) {
    foreach ($items as $topic) {
      $topics[] = 'node:' . $topic['target_id'];
    }
  }
  $topics_filter = new SolrFilterSubQuery('OR');
  foreach ($topics as $topic) {
    $topics_filter->addFilter('sm_field_topics', $topic);
  }
   
  // Group conditions together.
  $main_filter = new SolrFilterSubQuery('AND');
  $main_filter->addFilterSubQuery($content_type_filter);
  $issues_or_topics = new SolrFilterSubQuery('OR');
  $issues_or_topics->addFilterSubQuery($issues_filter);
  $issues_or_topics->addFilterSubQuery($topics_filter);
  $main_filter->addFilterSubQuery($issues_or_topics);
   /*
  // Create the query object and perform the request.
  try {
    // Create the query and then configure it.
    $query = apachesolr_drupal_query('apachesolr');
   
    // Specify that we only want nids in the result.
    $query->addParam('fl', 'entity_id');
    // Only return 4 matches.
    $query->addParam('rows', 12);
    // Add the above filters.
    $query->addFilterSubQuery($main_filter);
    // Sort by publish date in descending order.
    $sort_field = 'ds_field_publish_date_sort';
    $sort_direction = 'desc';
    $query->setAvailableSort($sort_field, $sort_direction);
    $query->setSolrsort($sort_field, $sort_direction);
   
    // Run query and render results if matches are found.
    list($final_query, $response) = apachesolr_do_query($query);
    if ($response->code == '200' && $response->response->numFound > 0) {
      // Extract nids from the response and load them.
      $nids = array();
      foreach ($response->response->docs as $result) {
        $nids[] = $result->entity_id;
      }
      if (in_array($avoid, $nids)) {
          $key = array_search($avoid, $nids);
          unset($nids[$key]);
        }
      $items = entity_load('node', $nids);
      if (count($items)) {

        // Build and return the list of rendered teasers.
        return $items;
      }else{*/

        $tags = !empty($node->field_tags_relacionados)?$node->field_tags_relacionados['und']:array(1);
        $targets_id = array();
        foreach($tags as $tag){
          $targets_id[] = $tag['target_id'];
        }
        /* busqueda organizada por la fecha de publicación limitada a 12*/
        $query = new EntityFieldQuery();
        $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', array('article', 'gallery'))
        ->entityCondition('entity_id', $avoid, '!=')
        ->propertyCondition('status', 1)
        ->propertyOrderBy('created', 'DESC')
        ->range(0, 4);
        if(!empty($targets_id)){
          $query ->fieldCondition('field_tags_relacionados', 'target_id', $targets_id);
          $result = $query->execute();
                //evalucación de vacio y carga de los contenidos
          if (isset($result['node'])) {
            $nids = array_keys($result['node']);
            $items = entity_load('node', $nids);
            return $items;
          }
        }

     /* }
    }
  }
  catch (Exception $e) {
    watchdog('mymodulename', 'There was an error while processing More Like This block: %error', array('%error' => $e->getMessage()), WATCHDOG_ERROR);
  }*/
}


function secciones_slider(){
  //esta funcion hace la busqueda y trae los nodos
  $items = secciones_ultimos();
  $items_render = array();
  //contenedor
  $items_render['container']=array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('cabezote-seccion'),
      'id' =>'related_content_slider'
      ),
       /*'#attached' => array(
            'js' => array(
                libraries_get_path('flexslider') . '/jquery.flexslider.js',
            ),
            ),}*/
            );
  //slides
  $items_render['container']['slides']=array(
    '#type' => 'markup',
    '#markup' => theme('item_list', array(
      'items' => nodos_agrupados($items),
      //'attributes'=>array('class'=>array('slides')),
      //'wrapper_attributes' => array('id' => 'related_content_slider', 'class' => array()),
      )),
    );

  $completado=drupal_render($items_render);

  return $completado;
}

function nodos_agrupados($items){

  //contador de nodos
  $howmany = 0;
  //contador de grupos
  $grupos  = 0;
  $nodos_agrupados = array();
  $contador = 0;
  if(!empty($items)){
    foreach ($items as $key => $value) {
      $contador++;

        //aplicando el viewmode;
      if ($contador <= 1) {
        $result = node_view($value,'relacionado_principal');
      }else{
        $result = node_view($value,'contenido_relacionado');
      }

      //se pone un dsiplay especial para el primero


        //pasando el markup para la lista se debe renderisar
      $salida_nodo = drupal_render($result);
      if (DEVICE_TYPE_ROTATOR_VIDEOS_CTV_HOME == 'phone') {
            //división en grupos de 2
        if ($howmany % 1 == 0){
          $grupos ++;
                //Array listo para theme list
          $nodos_agrupados[$grupos] = array(
           'data'  => $salida_nodo,
           'class' => array('dummy'=>'category'),
           );

        }else{

          $nodos_agrupados[$grupos]['data'] .= $salida_nodo;
        }
      }else{
            //división en grupos de 4
        if ($howmany % 4 == 0){
          $grupos ++;
                //Array listo para theme list
          $nodos_agrupados[$grupos] = array(
           'data'  => $salida_nodo,
           'class' => array('dummy'=>'category'),
           );

        }else{

          $nodos_agrupados[$grupos]['data'] .= $salida_nodo;
        }
      }
      $howmany++;
    }
  }
  return $nodos_agrupados;

}

