//cartel_secciones
(function($) {
  $(document).ready(function() {
    $(".field-name-field-sponsored-content .field-items .field-item").each(function(){
      var title = $(this).find("#node_article_destacados_home_general_group_texto_subtitulo .field-name-title h1 a").text();
      
      if (title.length >= 32) {



        var subtitulo = $(this).find("#node_article_destacados_home_general_group_texto_subtitulo  .field-name-field-subtitulo .field-item").text();

        var maxLength = 40; // maximum number of characters to extract

        //trim the string to the maximum length
        var trimmedString = subtitulo.substr(0, maxLength);

        trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")))


        $(this).find("#node_article_destacados_home_general_group_texto_subtitulo  .field-name-field-subtitulo .field-item").replaceWith(trimmedString+' ...');
      }
    });

  });
})(jQuery);