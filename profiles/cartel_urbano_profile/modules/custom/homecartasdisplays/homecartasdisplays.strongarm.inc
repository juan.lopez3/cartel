<?php
/**
 * @file
 * homecartasdisplays.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function homecartasdisplays_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_publicidad';
  $strongarm->value = 0;
  $export['comment_anonymous_publicidad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_widget';
  $strongarm->value = 0;
  $export['comment_anonymous_widget'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_publicidad';
  $strongarm->value = 1;
  $export['comment_default_mode_publicidad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_widget';
  $strongarm->value = 1;
  $export['comment_default_mode_widget'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_publicidad';
  $strongarm->value = '50';
  $export['comment_default_per_page_publicidad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_widget';
  $strongarm->value = '50';
  $export['comment_default_per_page_widget'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_publicidad';
  $strongarm->value = 1;
  $export['comment_form_location_publicidad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_widget';
  $strongarm->value = 1;
  $export['comment_form_location_widget'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_publicidad';
  $strongarm->value = '1';
  $export['comment_preview_publicidad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_widget';
  $strongarm->value = '1';
  $export['comment_preview_widget'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_publicidad';
  $strongarm->value = '2';
  $export['comment_publicidad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_publicidad';
  $strongarm->value = 1;
  $export['comment_subject_field_publicidad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_widget';
  $strongarm->value = 1;
  $export['comment_subject_field_widget'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_widget';
  $strongarm->value = '2';
  $export['comment_widget'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__publicidad';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'contenido_relacionado' => array(
        'custom_settings' => TRUE,
      ),
      'entityreference_view_widget' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'search_index' => array(
        'custom_settings' => FALSE,
      ),
      'search_result' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'agenda_home' => array(
        'custom_settings' => FALSE,
      ),
      'card_destacado_home' => array(
        'custom_settings' => FALSE,
      ),
      'destacados_home_general' => array(
        'custom_settings' => FALSE,
      ),
      'destacado_seccion' => array(
        'custom_settings' => FALSE,
      ),
      'detalle_sidebar' => array(
        'custom_settings' => FALSE,
      ),
      'detalle_split' => array(
        'custom_settings' => FALSE,
      ),
      'home_galeria_destacado' => array(
        'custom_settings' => FALSE,
      ),
      'home_galeria_destacado_mobile' => array(
        'custom_settings' => FALSE,
      ),
      'home_galeria_otros' => array(
        'custom_settings' => FALSE,
      ),
      'impresa' => array(
        'custom_settings' => FALSE,
      ),
      'miniatura_full_gente_home' => array(
        'custom_settings' => FALSE,
      ),
      'miniatura_galeria_home' => array(
        'custom_settings' => FALSE,
      ),
      'miniatura_gente' => array(
        'custom_settings' => FALSE,
      ),
      'miniatura_node_menu' => array(
        'custom_settings' => FALSE,
      ),
      'miniatura_node_section_home' => array(
        'custom_settings' => FALSE,
      ),
      'miniatura_opinion_home' => array(
        'custom_settings' => FALSE,
      ),
      'miniatura_vides_ctv_home' => array(
        'custom_settings' => FALSE,
      ),
      'muestra_1_columna' => array(
        'custom_settings' => FALSE,
      ),
      'muestra_1_columna_video' => array(
        'custom_settings' => FALSE,
      ),
      'muestra_2_columnas' => array(
        'custom_settings' => FALSE,
      ),
      'popular' => array(
        'custom_settings' => FALSE,
      ),
      'popular_internas' => array(
        'custom_settings' => FALSE,
      ),
      'slider_home' => array(
        'custom_settings' => FALSE,
      ),
      'slider_home_eventos' => array(
        'custom_settings' => FALSE,
      ),
      'slider_home_miniatura' => array(
        'custom_settings' => FALSE,
      ),
      'slider_home_mobile' => array(
        'custom_settings' => FALSE,
      ),
      'slider_home_mobile_destacado' => array(
        'custom_settings' => FALSE,
      ),
      'slider_home_nuevo' => array(
        'custom_settings' => FALSE,
      ),
      'revision' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'metatags' => array(
          'weight' => '40',
        ),
        'title' => array(
          'weight' => '-5',
        ),
        'path' => array(
          'weight' => '30',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__publicidad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__widget';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'contenido_relacionado' => array(
        'custom_settings' => TRUE,
      ),
      'entityreference_view_widget' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'search_index' => array(
        'custom_settings' => FALSE,
      ),
      'search_result' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'agenda_home' => array(
        'custom_settings' => FALSE,
      ),
      'card_destacado_home' => array(
        'custom_settings' => FALSE,
      ),
      'destacados_home_general' => array(
        'custom_settings' => FALSE,
      ),
      'destacado_seccion' => array(
        'custom_settings' => FALSE,
      ),
      'detalle_sidebar' => array(
        'custom_settings' => FALSE,
      ),
      'detalle_split' => array(
        'custom_settings' => FALSE,
      ),
      'home_galeria_destacado' => array(
        'custom_settings' => FALSE,
      ),
      'home_galeria_destacado_mobile' => array(
        'custom_settings' => FALSE,
      ),
      'home_galeria_otros' => array(
        'custom_settings' => FALSE,
      ),
      'impresa' => array(
        'custom_settings' => FALSE,
      ),
      'miniatura_full_gente_home' => array(
        'custom_settings' => FALSE,
      ),
      'miniatura_galeria_home' => array(
        'custom_settings' => FALSE,
      ),
      'miniatura_gente' => array(
        'custom_settings' => FALSE,
      ),
      'miniatura_node_menu' => array(
        'custom_settings' => FALSE,
      ),
      'miniatura_node_section_home' => array(
        'custom_settings' => FALSE,
      ),
      'miniatura_opinion_home' => array(
        'custom_settings' => FALSE,
      ),
      'miniatura_vides_ctv_home' => array(
        'custom_settings' => FALSE,
      ),
      'muestra_1_columna' => array(
        'custom_settings' => FALSE,
      ),
      'muestra_1_columna_video' => array(
        'custom_settings' => FALSE,
      ),
      'muestra_2_columnas' => array(
        'custom_settings' => FALSE,
      ),
      'popular' => array(
        'custom_settings' => FALSE,
      ),
      'popular_internas' => array(
        'custom_settings' => FALSE,
      ),
      'slider_home' => array(
        'custom_settings' => FALSE,
      ),
      'slider_home_eventos' => array(
        'custom_settings' => FALSE,
      ),
      'slider_home_miniatura' => array(
        'custom_settings' => FALSE,
      ),
      'slider_home_mobile' => array(
        'custom_settings' => FALSE,
      ),
      'slider_home_mobile_destacado' => array(
        'custom_settings' => FALSE,
      ),
      'slider_home_nuevo' => array(
        'custom_settings' => FALSE,
      ),
      'revision' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__widget'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_publicidad';
  $strongarm->value = '0';
  $export['language_content_type_publicidad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_widget';
  $strongarm->value = '0';
  $export['language_content_type_widget'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_publicidad';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_publicidad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_widget';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_widget'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_publicidad';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_publicidad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_widget';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_widget'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_publicidad';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
  );
  $export['node_options_publicidad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_widget';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
  );
  $export['node_options_widget'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_publicidad';
  $strongarm->value = '0';
  $export['node_preview_publicidad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_widget';
  $strongarm->value = '0';
  $export['node_preview_widget'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_publicidad';
  $strongarm->value = 1;
  $export['node_submitted_publicidad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_widget';
  $strongarm->value = 1;
  $export['node_submitted_widget'] = $strongarm;

  return $export;
}
