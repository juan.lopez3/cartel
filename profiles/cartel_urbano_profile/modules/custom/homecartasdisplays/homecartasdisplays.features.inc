<?php
/**
 * @file
 * homecartasdisplays.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function homecartasdisplays_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function homecartasdisplays_image_default_styles() {
  $styles = array();

  // Exported image style: destacada_card_home.
  $styles['destacada_card_home'] = array(
    'label' => 'destacada card home',
    'effects' => array(
      23 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 830,
          'height' => 375,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: home_card_otros.
  $styles['home_card_otros'] = array(
    'label' => 'home card otros',
    'effects' => array(
      24 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 400,
          'height' => 375,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: home_destacado_principal.
  $styles['home_destacado_principal'] = array(
    'label' => 'home destacado principal',
    'effects' => array(
      20 => array(
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 1000,
          'height' => 620,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: home_galeria_otros.
  $styles['home_galeria_otros'] = array(
    'label' => 'Home galería otros',
    'effects' => array(
      23 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 500,
          'height' => 500,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function homecartasdisplays_node_info() {
  $items = array(
    'publicidad' => array(
      'name' => t('Publicidad'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'widget' => array(
      'name' => t('Widget'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
