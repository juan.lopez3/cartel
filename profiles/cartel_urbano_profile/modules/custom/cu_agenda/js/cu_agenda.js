(function($, window, undefined) {
	$(document).ready(function() {

		form();

	});

	Drupal.behaviors.cu_agenda = {
		attach: function(context, settings) {
		  $(document).ajaxComplete(function(event, xhr, settings) {
		    //event.stopImmediatePropagation();
		    //event.stopPropagation();
		    form();
		  });
		}
	};


	function form(){
		
		$('button#edit-reset').click(function(){
			$('form#views-exposed-form-agenda-panel-pane-1').each(function(){

			  $("select#edit-field-fecha-value").children().removeAttr("selected");
			  $("select#edit-field-fecha-value option[value=All]").attr('selected','selected');

			  $("select#edit-field-tipo-de-evento-target-id").children().removeAttr("selected");
			  $("select#edit-field-tipo-de-evento-target-id option[value=All]").attr('selected','selected');

			  $("select#edit-field-ciudad-target-id").children().removeAttr("selected");
			  $("select#edit-field-ciudad-target-id option[value=All]").attr('selected','selected');

			});

			$('button#edit-submit-agenda').click();

			return false;
		});

		$(document).find("form#views-exposed-form-agenda-panel-pane-1").attr("action","");

	}

})(jQuery, window);
