<?php
/**
 * Run-time rendering of the body of the block (content type)
 * See ctools_plugin_examples for more advanced info
 */

$plugin = array(
  'title' => t('Proximos eventos home'),
  'category' => t('Cartel Urbano'),
  'render callback' => 'cu_agenda_home_render',
);

/**
 * Run-time rendering of the body of the block (content type)
 * See ctools_plugin_examples for more advanced info
 */
function cu_agenda_home_render($subtype, $conf, $args, $contexts) {

/*Bloque con el flexslider 
  Flexslider con el id del div que lo contiene 'otras secciones' es el nombre de sistema de la configuración de   flexslider aplicada
*/

  $block = new stdClass();
  $block->content = eventos_ultimos();

  return $block;

}
function eventos_ultimos(){
 
  /* busqueda organizada por la fecha de publicación tomando las proximas 4 semanas*/
 $currentdate = date("Y-m-d h:i:s", strtotime("+1 month"));
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
          ->entityCondition('bundle', 'eventos')
          ->fieldCondition('field_fecha', 'value', $currentdate, '<=');

   $result = $query->execute();

 //cache_set('eventos_del_mes',  $result);
  //evaluación de vacio y carga de los contenidos
  if (isset($result['node'])) {
    $ids = array_keys($result['node']);
    $items = entity_load('node', $ids);
  }
  $render['vertical_tabs'] = array(
      '#type' => 'horizontal_tabs',
      '#tree' => TRUE,
      '#attached' => array('library' => array(array('field_group', 'horizontal-tabs'))), // 
      '#prefix' => '<div id="unique-wrapper"> <h2><a href="eventos/agenda"> AGENDA //</a></h2>',
      '#suffix' => '</div>',
      '#attributes' => array(
        'class' => array('header-picoyplaca', 'another'),

      )
    );
  $monday = date("Y-m-d h:i:s",strtotime("last Monday"));
  $fechainicial = new DateTime($monday); 


  for ($i=0; $i < 4 ; $i++) { 
   
    $fsemana = $fechainicial->format('Y-m-d');
    $fsemMonday = $fechainicial->format('d');
    $fsemmonth = $fechainicial->format('F');
    $end = strtotime($fsemana);
    $fsem = date("d", strtotime("next Sunday, 11:59", $end));

    $render['vertical_tabs'][$i] = array(
      '#type' => 'fieldset',
      '#title' => ''.t($fsemmonth) .' '. $fsemMonday . '-'. $fsem,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
      '#weight' => -99,
    );

     $render['vertical_tabs'][$i][] = array(
        '#type' => 'markup',
        '#markup' => eventos_semana($fechainicial, $items),
        );

     $next = date("Y-m-d", strtotime("$fsemana + 7 days"));
     $fechainicial = new DateTime($next);

  }
  krsort($render['vertical_tabs']);
  $render['link'] = array(
      '#type' => 'markup',
      '#markup' => '<div class="view-footer">'.l('VER TODA LA AGENDA', 'eventos/agenda').'</div>',
    );
  return $render;
}

function eventos_semana($fechainicial, $items){

  $periodo = new DatePeriod($fechainicial, new DateInterval('P1D'), 7);
  $fechas = iterator_to_array($periodo);

  foreach ($fechas as $key => $value) {
    $fechas[$key] = $value->format('Y-m-d');
  }

    foreach ($items as $key => $value) {
      $eventdate = new DateTime($value->field_fecha['und'][0]['value']); 
      $eventdate = $eventdate->format('Y-m-d');
      if (in_array($eventdate, $fechas))
        {
          if(!isset($render[2])){
            $render[] = entity_view('node', array($value), 'agenda_home');
          }
        }
    }

  return drupal_render($render);

}