
(function($) {
  $(document).ready(function() {

if (typeof Drupal.settings.cu_redessociales === 'undefined') {
    return false;
}



    jQuery.each(Drupal.settings.cu_redessociales, function(index, value) {
      load_counts(index, value);
    });
  });

  $.fn.grid_content_loaded = function() {
    jQuery.each(Drupal.settings.cu_redessociales, function(index, value) {
      load_counts(index, value);
    });
  };


  $.fn.grid_content_loading = function() {
    jQuery.each(Drupal.settings.cu_redessociales, function(index, value) {
      load_counts(index, value);
    });
  };

  Drupal.behaviors.cu_redessociales = {
    attach: function(context, settings) {
      $(document).ajaxComplete(function(event, xhr, settings) {
          jQuery.each(Drupal.settings.cu_redessociales, function(index, value) {

            //load_counts(index, value);


          });
      });
     }
  }




function load_counts(index, value) {

    var url = value.url_node;
    var url_2 = value.url_short_node[0];
    var nid = value.nid;

    var datafb = 0;
    var datatw = 0;
    var datapt = 0;

    var checkdata;
    var checkfb;
    var checktw;
    var checkpt;

    checkdata = setTimeout(
            function()
            {
              var total = datafb + datatw + datapt;
              jQuery('#node-' + nid + ' span.count_total').html(total);
              jQuery('#node-' + nid + ' #compartido_contador_facebook').html(datafb);
              jQuery('#node-' + nid + ' #compartido_contador_twitter').html(datatw);
              jQuery('#node-' + nid + ' #compartido_contador_pinterest').html(datapt);

            }, 5000);

    // Facebook Shares Count
    jQuery.getJSON('http://graph.facebook.com/?id=' + window.location.href, function() {
    }).always(function(fbdata) {
        checkfb = true;
        datafb = fbdata.share.share_count;
    });

    // Twitter Shares Count
    jQuery.ajax({
      type: 'GET',
      dataType: 'jsonp',
      data: {},
      url: "http://urls.api.twitter.com/1/urls/count.json?url=" + url_2 + '&callback=?',
      error: function(jqXHR, textStatus, errorThrown) {
        //console.log(jqXHR);
      },
      success: function(twitdata) {
        if (typeof twitdata.count != 'undefined') {
          checktw = true;
          datatw = twitdata.count;
        }
      }
    });

    // Pinterest Shares Count
    jQuery.ajax({
      type: 'GET',
      dataType: 'jsonp',
      data: {},
      url: "http://api.pinterest.com/v1/urls/count.json?url=" + url_2,
      error: function(jqXHR, textStatus, errorThrown) {
        //console.log(jqXHR);
      },
      success: function(pintdata) {
        if (typeof pintdata.count != 'undefined') {
          checkpt = true;
          datapt = pintdata.count;
        }
      }
    });
}

})(jQuery);
