<div class="redes_sociales" id="node-<?php print $nid;?>">
<div class="msj-share">
    <?php 
    $var =  menu_get_object($type = 'node', $position = 1, $path = NULL);
        if (isset($var->nid)) {
            print "COMPARTIR ARTICULO EN:";
        }
     ?>
</div>
    <ul>            
        <li>
            <span class="count_total"></span>
            <ul>
                <li>
                    <div class="compartido_facebook">
                        <div class="count-share-content-facebook count-share-content compartido_contador" id="compartido_contador_facebook">
                        </div>
                        <div class="compartido_icono" id="compartido_icono_facebook">
                            <a class="icon-cartel-03" rel="nofollow" href="http://www.facebook.com/sharer/sharer.php?u=<?php print $url_corta ?>&amp;src=sp&amp;p[title]=<?php print $title ?>" target="_blank" name="fb_share" type="button" share_url="<?php if (!empty($url)): print $url ; endif; ?>"></a>
                        </div>
                    </div>            
                </li>
                <li>
                    <div class="compartido_twitter">
                        <div class="count-share-content-twitter count-share-content compartido_contador" id="compartido_contador_twitter">
                        </div>
                        <div class="compartido_icono" id="compartido_icono_twitter">
                            <a class="twShare icon-cartel-04" data-lang="es" href="http://twitter.com/intent/tweet?url=<?php print $url_corta ?>&via=<?php print $via ?>&hashtags=<?php if (!empty($hashtags)): print $hashtags ; endif; ?>&counturl=<?php if (!empty($url)): print $url ; endif; ?>&text=<?php print $title ?>&count='vertical'"  target="_blank"></a>                    
                        
                        </div>

                    </div>     
                </li>
                <?php if(!empty($path_img)) : ?>
                <li>
                    <div class="compartido_pinterest">
                        
                        <div class="count-share-content-pinterest count-share-content compartido_contador" id="compartido_contador_pinterest">
                        </div>
                        <div class="compartido_icono" id="compartido_icono_pinterest">
                            <a href="http://www.pinterest.com/pin/create/button/?url=<?php print $url ?>&media=<?php if (!empty($path_img)): print $path_img ; endif; ?>&description=<?php print $title ?> vía <?php print $via ?>" data-pin-do="buttonPin" data-pin-config="above" target="_blank">P</a>
                        </div>
                        
                    </div> 
                </li> 
                <?php endif;?>
            </ul>
        </li>
    </ul>
</div>
