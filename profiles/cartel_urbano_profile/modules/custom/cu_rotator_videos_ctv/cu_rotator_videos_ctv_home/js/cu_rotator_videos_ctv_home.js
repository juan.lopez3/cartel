

(function($) {
    $(document).ready(function() {
        //agregando link para que cuando se de click en la video me lleva a la pagina de cartel tv
        //envindo como parametro el id del nodo del video
        $('ul.slides li div.node-video').css({'cursor': 'pointer'}).click(function() {

            var id_node = $(this).attr('id').split("-")[1];

            window.location = Drupal.settings.basePath + 'carteltv?nid_v=' + id_node;
        });

    });
})(jQuery);
