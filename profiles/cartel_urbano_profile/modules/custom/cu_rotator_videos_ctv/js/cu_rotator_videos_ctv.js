(function($) {
    $(window).load(function() {
        // cargando autoplay del video de youtube que se es viendo, se toma el id del video del la url
        jQuery('div#edit-field-seccion-tid-all').css({'display': 'none'});
         
        var nid_v = getUrlVars()['nid_v'];
        
        var src = $('li#node-' + nid_v + ' iframe').attr('src');
        $('li#node-' + nid_v + ' iframe').attr('src', src + '&autoplay=1');
        $('li.clone#node-' + nid_v + ' iframe').attr('src', src);


    });
})(jQuery);



function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}