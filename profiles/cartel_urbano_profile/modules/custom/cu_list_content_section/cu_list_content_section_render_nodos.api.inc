<?php

/**
 * Realiza la consulta a la bd
 * con los tipo de bloque se llaman funciones aparte y co cada una de ellas se realizan la s especificaciones de cada bloque
 * es decir si es de tipo uno se revisa si trae un nodo doble columna se le cambia el view mode
 * si es de tipo 2 se consulta los nodos de bocadillo y por medio de un random se ubica en una posicion del array
 * y si es de tipo 3 por medio de un random se decide cual se vera de dos columnas cuidadnod que no sea el numero 3
 * para q no descuadre el diseño
 * 
 * por ultimo se retorna el array de los nodos con los view mode seleccionados dependiendo de su situacion
 */
function _list_content_section($type, $section, $pager) {

  $path_module = drupal_get_path('module', 'cu_redessociales') . '/js';

  drupal_add_js($path_module . '/cu_redessociales.js');

  // $type = 2;

  $queue = nodequeue_load_queue_by_name('secciones');
  
  
  $squeues = nodequeue_load_subqueues_by_queue($queue->qid);
  
  foreach ($squeues as $key => $squeue) {
    
    if($squeue->reference == $section){
      $sqid = $squeue->sqid;
    }
  }
  
  $nodequeue_load_nodes = nodequeue_load_nodes($sqid);
  
 
  $nodeq = isset($nodequeue_load_nodes[0]->vid) ? $nodequeue_load_nodes[0]->vid : null;
   

  $consult = _consult_node($type, $section, $pager, $nodeq);

  $count_consult = count($consult['node']);
 
  $_SESSION['section_total_count'] = $_SESSION['section_total_count'] - $count_consult;

  if($count_consult == 0) return "fin";

  $nids = isset($consult['node']) ? array_keys($consult['node']) : null;
  $output = '';

  switch ($type):
    case 1:
      $nodos_temp = entity_load('node', $nids);
      $nodos = type_one($nodos_temp, $nids);
      $_SESSION['active_show_d_column'] = $nodos[0];
      if ($_SESSION['active_show_d_column'] == 1) {
        array_pop($nodos);
      }
      unset($nodos[0]);
      break;
    case 2:
      $nids = type_two($nids);
    case 3:
      $node_double_column = type_three($nids);
      break;
  endswitch;

  if (!isset($nodos)) {
    $nodos = entity_load('node', $nids);
  }

  foreach ($nodos as $nodo) {
    $view_mode = 'destacados_home_general';

    if (isset($node_double_column[0]) && $nodo->nid == $node_double_column[0] || isset($_SESSION['nodo_active']) && $nodo->nid == $_SESSION['nodo_active']) {

      $nid = $nodo->nid;


      $url = drupal_get_path_alias('node/' . $nid);
      if ($url != NULL) {

        $url = url($url, array('absolute' => TRUE, 'alias' => !variable_get('shorten_use_alias', 1)));

        $url_corta = shorten_url($url);

        drupal_add_js(array('cu_redessociales' => array($nid => (array(
            'url_short_node' => array($url_corta),
            'url_node' => array($url),
            'nid' => array($nid),
                )))), 'setting');
      }

      if ($type != 2) {
        $view_mode = 'card_destacado_home';
      }
    }

    $x = node_view($nodo, $view_mode);
    $output .= drupal_render($x);
  }

  return $output;
}