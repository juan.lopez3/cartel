
(function($) {
  $(document).ready(function() {

    $('a#ajax_link').click(function(e) {

      $.ajax(
              {
                url: Drupal.settings.basePath+'/ajax_link_callback_ver_mas/ajax/',
                type: "POST",
                data: { type: "ajax"} ,
                crossDomain: true,
                success: function(data, textStatus, jqXHR)
                {
                  //console.log(data);
                  
                  $('#div_more').append(data);
                  
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                  console.log(errorThrown);
                }
              });
      e.preventDefault();
    });
  });
})(jQuery);