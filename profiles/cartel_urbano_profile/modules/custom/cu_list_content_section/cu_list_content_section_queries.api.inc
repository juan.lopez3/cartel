<?php

function _consult_node($type, $section, $pager, $nid_queue) {

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node', '=')
          ->entityCondition('bundle', array(TYPE_CONTENT_1, TYPE_CONTENT_2, TYPE_CONTENT_3))
          ->propertyCondition('status', 1)
          ->propertyOrderBy('created', 'DESC')
          ->fieldCondition('field_seccion', 'tid', $section);


  if ($nid_queue != null)
    $query->propertyCondition('nid', $nid_queue, '!=');



  $pager = (!$pager)?0:$pager;

  $inicio = isset($_SESSION["cargados_ver_mas"])?$_SESSION["cargados_ver_mas"]: (ELEMENTS_BY_PAGE * $pager);
  $fin = ELEMENTS_BY_PAGE;

  switch ($type):
  	// traemos ELEMENTS_BY_PAGE nodos
    


	case 1:

    case 2:

    break;
    case 3:

		//if (isset($_SESSION['active_show_d_column']) && $_SESSION['active_show_d_column'] == 1) {

			

			$fin = ELEMENTS_BY_PAGE-1;
		
		//}


      break;
  endswitch;

  $query->range($inicio, $fin);


  $_SESSION["cargados_ver_mas"] = $inicio + $fin;




  if (isset($cache_get_list_content_section)) {
    $cache_get_list_content_section = cache_get('list_content_section');
  } else {
    $result = $query->execute();
    cache_set('list_content_section', $result);
  }


  if ($result && !empty($result['node'])) {
    return $result;
  }
}

function _consult_node_bocadillo($count) {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node', '=')
          ->entityCondition('bundle', TYPE_CONTENT_BOCADILLO, '=')
          ->propertyCondition('status', 1)
          ->propertyOrderBy('created', 'DESC')
          //->addTag('random')
          ->range($count, 1);

  if (isset($cache_get_list_content_section_bocadillo)) {
    $cache_get_list_content_section_bocadillo = cache_get('list_content_section_bocadillo');
  } else {
    $result = $query->execute();
    cache_set('list_content_section_bocadillo', $result);
  }


  if ($result && !empty($result['node'])) {
    return $result;
  }
}

function _consult_node_count($section, $nid_queue = NULL ) {

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node', '=')
          ->entityCondition('bundle', array(TYPE_CONTENT_1, TYPE_CONTENT_2, TYPE_CONTENT_3))
          ->propertyCondition('status', 1)
          ->propertyOrderBy('created', 'DESC')
          ->fieldCondition('field_seccion', 'tid', $section);
  if ($nid_queue != null)
    $query->propertyCondition('nid', $nid_queue, '!=');


  if (isset($cache_get_list_content_section)) {
    $cache_get_list_content_section = cache_get('list_content_section_count');
  } else {
    $result = $query->count()->execute();
    cache_set('list_content_section_count', $result);
  }

  if ($result && !empty($result)) {
    return $result;
  }
}