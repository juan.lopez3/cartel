<?php

function type_one($nodos, $nids) {
  $pos_nodo = array();
  $active_show_d_column = 0;
  foreach ($nodos as $key => $nodo) {
    if (isset($nodo->field_show_d_column['und'][0]['value']) && $nodo->field_show_d_column['und'][0]['value'] == 1) {
      $active_show_d_column = 1;
      $_SESSION['nodo_active'] = $nodo->nid;
      if ($nids[2] == $key) {
        $position = 2;
      }
      if ($nids[1] == $key) {
        $position = 1;
      }
    }
    $pos_nodo[] = $nodo;
  }
  if ($active_show_d_column == 1) {
    if (isset($position) && $position == 2) {
      $temp = $pos_nodo[2];
      $pos_nodo[2] = $pos_nodo[3];
      $pos_nodo[3] = $temp;
    } else if (isset($position) && $position == 1 && DEVICE_TYPE == 'phone') {
      $temp = $pos_nodo[1];
      $pos_nodo[1] = $pos_nodo[2];
      $pos_nodo[2] = $temp;
    }
  }

  array_unshift($pos_nodo, $active_show_d_column);
  return $pos_nodo;
}

function type_two($nids) {
  if (isset($_SESSION['pager_section_custom_type_2'])) {
    $_SESSION['pager_section_custom_type_2'] = $_SESSION['pager_section_custom_type_2'] + 1;
  } else {
    $_SESSION['pager_section_custom_type_2'] = 0;
  }
  //$consul_bocadillo = _consult_node_bocadillo($_SESSION['pager_section_custom_type_2']);
  $r = rand(0, 6);
  //array_splice($nids, $r, 0, array_keys($consul_bocadillo['node']?$consul_bocadillo['node']:array()));
  return $nids;
}

function type_three($nids) {
  $r_t3 = rand(0, 4);
  if ($r_t3 % 3 != 0) {
    if ($r_t3 % 2 != 0) {
      
    } else {
      $r_t3 = rand(3, 4);
    }
  } else {
    $r_t3 = rand(0, 1);
  }
  if(isset($nids[$r_t3])){
    $node_double_column = $nids[$r_t3];
    return array($node_double_column);
  }  
}

