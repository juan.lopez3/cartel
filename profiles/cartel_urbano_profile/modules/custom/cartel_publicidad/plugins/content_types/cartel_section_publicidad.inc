<?php
$plugin = array(
  'single' => TRUE,  // Just do this one, it is needed.
  'title' => t('Publicidad'),  // Title to show up on the pane screen.
  'category' => t('Cartel Urbano'), // A category to put this under.
  'edit form' => 'cartel_publicidad_edit_form', // A function that will return the settings form for the pane.
  'render callback' => 'cartel_publicidad_render', // A function that will return the renderable content.
  'defaults' => array( // Array of defaults for the settings form.
    'selected' => '1',
  ),
  'all contexts' => TRUE,
  );


/**
 * Run-time rendering of the body of the block (content type)
 * See ctools_plugin_examples for more advanced info
 */
function cartel_publicidad_render($subtype, $conf, $args, $contexts) {
  $block = new stdClass();
  $block->content = _cartel_publicidad_block($subtype, $conf, $args, $contexts);
  return $block;
}

function _cartel_publicidad_block($subtype, $conf, $args, $contexts) {
  
  $torender='';
  $entityidtransit=array($conf['selected']);
  $entitype = 'publicidad';
  $torender = entity_load($entitype, $entityidtransit);

  if(isset($torender[$conf['selected']]->field_script['und'][0]['value'])) { 
    if(isset($torender[$conf['selected']]->field_imagen['und'][0]['uri'])) {
      $bg_uri = $torender[$conf['selected']]->field_imagen['und'][0]['uri'];
      $url = file_create_url($bg_uri);
    }
      $scripti = $torender[$conf['selected']]->field_script['und'][0]['value'];
      $block = theme('cartel_publicidad_home', 
        
        array(
          'scripti' => $scripti, 
          'seccion' => isset($url)?$url:null)
        );    

      return  $block;
  }
}

/**
 * An edit form for the pane's settings.
 */

function cartel_publicidad_edit_form($form, &$form_state) {
 
  $query = new EntityFieldQueryExtraFields();
  $query->entityCondition('entity_type', 'publicidad')
  ->addExtraField('', 'title', 'title', 'eck_publicidad');
  $result=$query->execute();
  $publicidad = $result['publicidad'];
  foreach ($publicidad as $key => $value) {
      $options[$value->id] = $value->extraFields->title;    
  }  
  $conf = $form_state['conf'];

  $form['selected'] = array(
       '#type' => 'select',
       '#title' => t('Publicidad'),
       '#options' => $options,
       '#default_value' => $conf['selected'],
       '#description' => t(''),
   );
  return $form;
}
 
/**
 * Submit function, note anything in the formstate[conf] automatically gets saved
 * Notice, the magic that automatically does that for you.
 */

function cartel_publicidad_edit_form_submit(&$form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];

    }

  }

}
