<div id="<?php if (!empty($block_html_id)): print $block_html_id;
endif;
?>" class="<?php print $classes; ?>"<?php print $attributes; ?> <?php if (!empty($data_url_bg)): ?> data-uribg="<?php print $data_url_bg; ?>"<?php endif; ?>>

<?php print render($title_prefix); ?>

  <div class="header-section">
    <h2><a class="link_review" href="<?php print $url_term; ?>"><?php print $name_term; ?></a></h2>
    <p><?php print $description_term; ?></p>
  </div>
<?php print render($title_suffix); ?>

  <div class="content"<?php print $content_attributes; ?>>
<?php print $content ?>
  </div>

  <div class="view-footer">
    <a href="<?php print $url_term; ?>" class="link_inter_columnists">VER TODAS LAS COLUMNAS</a>
  </div>
</div>
