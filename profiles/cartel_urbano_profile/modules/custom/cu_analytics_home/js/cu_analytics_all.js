//cu_analytics_all
(function($) {

  $(document).ready(function() {

    /* medir el número de veces que dan clic en el menu o en cada sección del menu */
    cu_analytics_home_menu_item_click();

  });

/*

  Envia los datos a tagmanager

  */
  function cu_analytics_home_menu_item_click(){

  //Medir los clicks en las secciones del menu
  var menu = $("body").find("#block-system-main-menu .menu.nav");
  var text = '';
  menu.on("click", "li a", function(){
    text = $(this).text();
    dataLayer.push({
      'menu-text': text,
      'event': 'menu-item-clicked'
    });
  });

  //Medir las veces que abren o cierran el menu
  var menu_button = $(".abrir-menu.icon-cartel-ico-menu");
  var container_menu = $("#block-system-main-menu");
  menu_button.on("click", function(){
    //cierran menu
    if (!container_menu.hasClass("inactive")) {
      dataLayer.push({
        'event': 'cerrar-menu'
      });
      //abren menu
    }else if (container_menu.hasClass("inactive")) {
      dataLayer.push({
        'event': 'abrir-menu'
      });    
    }

  });


}


})(jQuery);