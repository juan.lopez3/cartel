//cu_analytics_home
(function($) {

  $(document).ready(function() {
    window.obji = new Array(); 

    /* Funcion para enviar promotion impression y promotion clicks */
    cu_analytics_home_add_node_id();
  });
//agrega las posiciones a los nodos
function cu_analytics_home_add_node_id(){

  var count_position = 0;
  var first_view_promoted  = $('.view.view-sliderhome.view-id-sliderhome.view-display-id-block');
  var second_view_promoted = $('.view.view-nodequeue-5.view-id-nodequeue_5.view-display-id-block');



//add count first view
first_view_promoted.find('.views-row').each(function(i, obj){
  count_position++;

  var id = $(this).find('img:first').attr("alt");
  var name = $(this).find('img:first').attr("alt");
  var creative = $(this).find('img:first').attr("alt");
  var position = count_position;

  var obj_click = Array();

  obj_click['id'] = id;
  obj_click['name'] = name;
  obj_click['creative'] = creative;
  obj_click['position'] = position;




  cu_analytics_home_push_element(id, name, creative, position);
  $(this).click(function(){
    var href = $(event.target).closest('a').attr('href');
    obj_click['destinationUrl'] = href;
    cu_analytics_home_on_promo_click(obj_click);
  });
});


//add count second view
second_view_promoted.find('.views-row').each(function(i, obj){
  count_position++;

  var id = $(this).find('img:first').attr("alt");
  var name = $(this).find('img:first').attr("alt");
  var creative = $(this).find('img:first').attr("alt");
  var position = count_position;

  var obj_click = Array();

  obj_click['id'] = id;
  obj_click['name'] = name;
  obj_click['creative'] = creative;
  obj_click['position'] = position;

  

  if (typeof obj == 'undefined' && count_position == 2) {

    id = 'publicidad';
    name = 'publicidad';
    creative = 'publicidad';
  }else if(count_position == 7){
    id = 'popular';
    name = 'popular';
    creative = 'popular';
  }

//  console.log(id + ' - ' + name + ' - ' + creative +' - ' + count_position);

cu_analytics_home_push_element(id, name, creative, position);

  $(this).click(function(){
    var href = $(event.target).closest('a').attr('href');
    obj_click['destinationUrl'] = href;
    cu_analytics_home_on_promo_click(obj_click);
  });
});

}

function cu_analytics_home_push_element(id, name, creative, position){
 dataLayer.push({

    'ecommerce': {

        'promoView': {

            'promotions': [                    

             {
                 'id': id,             

                 'name': name,

                 'creative': creative,

                 'position': position

             },]

        }

    }

});

}




function cu_analytics_home_on_promo_click(promoObj) {

    dataLayer.push({

        'event': 'promotionClick',

        'ecommerce': {

            'promoClick': {

                'promotions': [

                 {

                     'id': promoObj.id,                        

                     'name': promoObj.name,

                     'creative': promoObj.creative,

                     'position': promoObj.position

                 }]

            }

        },


        'eventCallback': function() {
      //     if (promoObj.position == 7) {
      //       document.location = '';
      //     }else{ 
      //       document.location = promoObj.destinationUrl;
      //     }

        }

    });


}

})(jQuery);