<?php
/**
 * @file
 * Easy Social view handler.
 */

/**
 * Handler for displaying Easy Social share buttons in views.
 */

class icck_redessociales_handler_field extends views_handler_field {
	
	
  function init(&$view, &$options) {
      parent::init($view, $options);
      
      // Declare additional fields to be loaded with a single query.
      // This way there's no need to node_load() on render().
      $this->additional_fields['nid'] = 'nid';
      $this->additional_fields['type'] = 'type';
      $this->additional_fields['language'] = 'language';
      $this->additional_fields['title'] = 'title';
  }
  
  //array('identifier' => array('table' => tablename, 'field' => fieldname); 

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  /**
   * Render field.
   */
  function render($values) {
    // All $additional_fields are aliased by Views.
    // Dealias & create a partial Node object.
    $node = new stdClass;
    $node->nid = $values->{$this->aliases['nid']};
    $node->type = $values->{$this->aliases['type']};
    $node->language = $values->{$this->aliases['language']};
    $node->title = urlencode($values->{$this->aliases['title']});

	$url = url('node/'.$node->nid, array('absolute' => TRUE));
	if(!empty($url)){
    $url_corta = shorten_url($url);
  }
  else{
    $url_corta = '';
  }
	$title =  urlencode($node->title);	
	$uri = url('node/'.$node->nid);
	
    return theme('compartir_enlaces', array('url' => $url,'title'=>$node->title,'lang' => NULL, 'url_corta'=>$url_corta, 'uri'=>$uri, 'nodo'=>$node));

  }

  /**
   * Define extra Easy Social field options.
   */
  function option_definition() {
    $options = parent::option_definition();
   
    //$options['easy_social_type'] = array('default' => EASY_SOCIAL_WIDGET_HORIZONTAL);
    //$options['easy_social_widgets'] = array('default' => variable_get_value('easy_social_global_widgets'));
    return $options;
  }

  /**
   * Extra Easy Social field options form.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    return;
    
  }

  /**
   * Extra Easy Social options submit callback.
   */
  function options_submit(&$form, &$form_state) {
    parent::options_submit($form, $form_state);
    return;
    //$form_state['values']['options']['easy_social_type'] = $form_state['values']['options']['easy_social']['easy_social_type'];
    //$form_state['values']['options']['easy_social_widgets'] = $form_state['values']['options']['easy_social']['easy_social_widgets'];
    //unset($form_state['values']['options']['easy_social']);
  }
}
