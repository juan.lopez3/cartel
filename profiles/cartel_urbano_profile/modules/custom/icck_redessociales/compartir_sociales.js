/* Esconder o Mostrar los enlaces de compartir en redes sociales*/
(function ($) {

  $(document).ready(function(){
	$(".compartir").click(function(e){
		e.stopImmediatePropagation();
		if ($(this).hasClass("activo")){
			$(this).removeClass("activo");		
		}else{
			$(this).addClass("activo");	
		}
	})

	$(document).click(function(){
		$(".compartir").removeClass("activo");	
	});


	$( ".view-goleadores-historicos .view-content" ).accordion(
		{ header: ".node .field-name-title",
		  collapsible: true 
	});


  });

  	

})(jQuery);
