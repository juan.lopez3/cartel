<?php
/**
 * @file
 * Easy Social Views hooks.
 */

/**
 * Implements hook_views_data().
 */
function icck_redessociales_views_data() {
  $data['node']['icck_redessociales'] = array(
    'field' => array(
      'title' => t('mocion redes sociales'),
      'help' => t('Muestra el  widget de compartir en redes sociales.'),
      'handler' => 'icck_redessociales_handler_field',
    ),
  );
  return $data;
}
