var rsociales = (function(window, $) {
    // private variables and functions
    var foo = 'bar';
    var twitter_count = 0;

    // constructor
    var module = function() {
    };

    // prototype
    module.prototype = {
        constructor: module,
        contar_twitter: function() {

            jQuery.getJSON("http://urls.api.twitter.com/1/urls/count.json?url=" + window.location.href + "&callback=?",
                    function(data) {
                        //Twitter
                        twitter_count = (data.count) ? data.count : "";
                        //Total
                        var twcount = (data.count) ? data.count : 0;


                        console.log(twcount);

                        jQuery(".nuevo-despliegue-redes-sociales .compartido_twitter #compartido_contador_twitter").html(twitter_count);
                        jQuery(".nuevo-despliegue-redes-sociales .compartido_twitter").show();
                    });

        }
    };
    return module;
})(window, jQuery);

jQuery(document).ready(function() {

    sociales = new rsociales();
    sociales.contar_twitter();
})
