<?php
use phpforms\Form;
use phpforms\Validator\Validator;

/* =============================================
    start session and include form class
============================================= */
session_start();
include_once '../Form.php';
/* =============================================
    validation if posted
============================================= */
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    include_once '../Validator/Validator.php';
    include_once '../Validator/Exception.php';
    $validator = new Validator($_POST);
    $required = array('username', 'useremail', 'myRadioBtnGroup', 'myCheckboxGroup');
    foreach ($required as $required) {
        $validator->required()->validate($required);
    }
    $validator->email()->validate('useremail');
    $validator->min(7)->max(15)->validate('userpass');
    /* ============================================================
    Errors are stored in session, so if the form is sent to a validation page,
    and the validation failed sends back to the form with header(),
    the errors will be displayed.
    ============================================================ */
    if ($validator->hasErrors()) {
        $_SESSION['errors']['my-form-id'] = $validator->getAllErrors();
    }
}
/* ==================================================
    for class and methods documentation,
    go to documentation/index.html
================================================== */
if (isset($_GET['vertical'])) {
     // just for vertical demo
    $form = new Form('my-form-id', 'vertical');
    $form->startFieldset('My vertical form');
} else {
    $form = new Form('my-form-id');
    $form->startFieldset('My horizontal form');
}
$form->addInput('text', 'username', '', 'Please enter your name : ', 'id=username-id, placeholder=My Placeholder Text, required=required');
$form->addInput('email', 'useremail', '', 'Please enter your email : ', 'placeholder=Your email here, required=required');
$form->addHtml('<p class="help-block">Your password must contain 7 characters min. and and 15 characters max.</p>', 'userpass', 'after');
$form->addInput('password', 'userpass', '', 'Please enter your password : ', 'required=required, pattern=(.){7\,15}');
if (!isset($_GET['vertical'])) {
    $form->addHtml('<div class="col-sm-12 alert alert-info">Input grouped on same line with placeholder :</div>');
    $form->groupInputs('street', 'zip', 'country');
    $form->setOptions(array('horizontalElementCol' => 'col-sm-3'));
    $form->addInput('text', 'street', '', 'Your adress : ', 'placeholder=street,required=required');
    $form->setOptions(array('horizontalLabelCol' => '', 'horizontalOffsetCol' => '', 'horizontalElementCol' => 'col-sm-2'));
    $form->addInput('text', 'zip', '', '', 'placeholder=zip code,required=required');
    $form->setOptions(array('horizontalElementCol' => 'col-sm-3'));
    $form->addOption('country', '', 'Country');
    $form->addOption('country', 'United States', 'United States');
    $form->addOption('country', 'Canada', 'Canada');
    $form->addOption('country', 'France', 'France');
    $form->addSelect('country', '', '');
    $form->setOptions(array('horizontalLabelCol' => 'col-sm-4', 'horizontalOffsetCol' => 'col-sm-4', 'horizontalElementCol' => 'col-sm-8'));
}
$form->addTextarea('my-textarea', 'Enter your message here ...', 'Your message : ', 'cols=30, rows=4');
$form->addPlugin('word-character-count', '#my-textarea', 'default', array('%maxAuthorized%' => 100));

for ($i = 1; $i < 6; $i++) {
    $form->addOption('mySelectName', $i, 'option ' . $i, 'group 1');
}
for ($i = 6; $i < 11; $i++) {
    $form->addOption('mySelectName', $i, 'option ' . $i, 'group 2');
}
$form->addSelect('mySelectName', 'Select please : ', '');
for ($i = 1; $i < 11; $i++) {
    $form->addOption('myMultipleSelectName', $i, 'option ' . $i);
}
$form->addSelect('myMultipleSelectName', 'Select please :<br>(multiple selections)', 'multiple=multiple');
$form->addRadio('myRadioBtnGroup', 'choice one', 'value 1');
$form->addRadio('myRadioBtnGroup', 'choice two', 'value 2');
$form->addRadio('myRadioBtnGroup', 'choice three', 'value 3');
if (!isset($_SESSION['myRadioBtnGroup'])) {
    $_SESSION['myRadioBtnGroup'] = 'value 3';
}
$form->printRadioGroup('myRadioBtnGroup', 'Choose one please : ', $inline = true, $attr = 'required=required');
$form->addCheckbox('myCheckboxGroup', 'choice one', 'choice-1', 'value 1');
$form->addCheckbox('myCheckboxGroup', 'choice two', 'choice-2', 'value 2');
$form->addCheckbox('myCheckboxGroup', 'choice three', 'choice-3', 'value 3');
if (!isset($_SESSION['myCheckboxGroup'])) {
    $_SESSION['choice-2'] = 'value 2';
}
$form->printCheckboxGroup('myCheckboxGroup', 'Check please : ', $inline = true, $attr = 'required=required');
$form->addInputWrapper('<div class="row"><div class="col-lg-4"></div></div>', 'myInputLg4');
$form->addInput('text', 'myInputLg4', '', 'input width : ');
$form->addInputWrapper('<div class="input-group"></div>', 'inputPrepend');
$form->addHtml('<div class="input-group-addon">@</div>', 'inputPrepend', 'before');
$form->addInput('text', 'inputPrepend', '', 'prepended text : ');
$form->addInputWrapper('<div class="input-group"></div>', 'inputAppend');
$form->addHtml('<div class="input-group-addon">€</div>', 'inputAppend', 'after');
$form->addInput('text', 'inputAppend', '', 'appended text : ');
$form->endFieldset();
$form->startFieldset('Plugins');
$form->addInput('text', 'defaultColorPicker', '', 'ColorPicker : ');
$form->addPlugin('colorpicker', '#defaultColorPicker');
$form->addInput('text', 'myCustomColorPicker', '', 'custom ColorPicker : ');
$myCustomXmlReplacement = array('%css-property%' => 'color');
$form->addPlugin('colorpicker', '#myCustomColorPicker', 'custom', $myCustomXmlReplacement);
$form->addInput('text', 'myTimePicker', '', 'TimePicker : ');
$form->addPlugin('timepicker', '#myTimePicker');
$form->addInput('text', 'myDatePicker', '', 'DatePicker : ');
$form->addPlugin('datepicker', '#myDatePicker');
$fileUpload_config = array('xml' => 'images', 'uploader' => 'imageFileUpload.php', 'btn-text' => 'Browse ...', 'max-number-of-files' => 2);
$form->addFileUpload('file', 'files[]', '', 'FileUpload : <br><span class="help-block">Upload for images - 2 images max.</span>', 'id=myFileUpload', $fileUpload_config);
$form->addInput('text', 'captcha', '', 'Type the characters please :', 'size=15');
$form->addPlugin('captcha', '#captcha', 'custom');
$form->addBtn('button', 'myBtnName', 1, 'Button alone', 'class=btn btn-primary');
$form->addBtn('button', 'myBtnCancelName', 1, 'Cancel', 'class=btn btn-danger, onclick=alert(\'cancelled\');', 'myBtnGroup');
$form->addBtn('submit', 'myBtnSubmitName', 1, 'Submit', 'class=btn btn-success', 'myBtnGroup');
$form->printBtnGroup('myBtnGroup');
$form->endFieldset();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>complete form example</title>
        <meta charset="UTF-8">
        <meta name=description content="">
        <meta name=viewport content="width=device-width, initial-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Demo styles -->
        <link rel="stylesheet" href="css/demo-styles.css">
        <!-- Google font for demo -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
        <!-- Bootstrap CSS -->
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <?php
$form->printIncludes('css'); ?>
    </head>
    <body>
        <header id="demo-header">
<?php
if (isset($_GET['vertical'])) {
     // just for vertical demo
    $title = 'Complete Vertical Form';
    $vertical_btn_class = 'btn-primary';
    $horizontal_btn_class = 'btn-default';
} else {
    $title = 'Complete Horizontal Form';
    $vertical_btn_class = 'btn-default';
    $horizontal_btn_class = 'btn-primary';
}
?>
            <a href="http://codecanyon.creation-site.org/phpforms/documentation/index.html" id="examples-header-logo"><img src="http://codecanyon.creation-site.org/phpforms/documentation/class-doc/images/phpforms-small-preview.png" alt="PHP Form Class Documentation" class="img-rounded pull-left"></a>
            <h1 class="text-center"><?php echo $title; ?></h1>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-lg-6 col-lg-offset-3 text-center">
                    <div class="btn-group text-center">
                        <a href="bootstrap-contact-form.php" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-play prepend"></span>Bootstrap Contact Form</a>
                        <a href="kube-contact-form.php" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-play prepend"></span>Kube Contact Form</a>
                        <a href="skeleton-contact-form.php" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-play prepend"></span>Skeleton Contact Form</a>
                        <a href="complete-form.php" class="btn <?php echo $horizontal_btn_class; ?> btn-sm"><span class="glyphicon glyphicon-play prepend"></span>Complete Horizontal Form</a>
                        <a href="complete-form.php?vertical=true" class="btn <?php echo $vertical_btn_class; ?> btn-sm"><span class="glyphicon glyphicon-play prepend"></span>Complete Vertical Form</a>
                    </div>
                </div>
            </div>
        </header>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-lg-6 col-lg-offset-3">
                    <a href="http://codecanyon.creation-site.org/phpforms/documentation/index.html" class="btn btn-primary btn-sm pull-right"><span class="glyphicon glyphicon-arrow-left prepend"></span>Back to Documentation</a>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs">
                      <li<?php if ($_SERVER["REQUEST_METHOD"] != "POST") echo ' class="active"'; ?>><a href="#code" data-toggle="tab">PHP Code</a></li>
                      <li<?php if ($_SERVER["REQUEST_METHOD"] == "POST") echo ' class="active"'; ?>><a href="#result" data-toggle="tab">Result</a></li>
                      <li><a href="#html" data-toggle="tab">HTML Result</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div class="tab-pane<?php if ($_SERVER["REQUEST_METHOD"] != "POST") echo ' active'; ?>" id="code">
                        <pre class="prettyprint"><?php echo htmlspecialchars('
                        <?php
                        /* =============================================
                            start session and include form class
                        ============================================= */
                        use phpforms\Form;
                        use phpforms\Validator\Validator;

                        session_start();
                        include_once(\'../Form.php\');
                        /* =============================================
                            validation if posted
                        ============================================= */
                        if ($_SERVER["REQUEST_METHOD"] == "POST") {
                            include_once(\'../Validator/Validator.php\');
                            include_once(\'../Validator/Exception.php\');
                            $validator = new Validator($_POST);
                            $required = array(\'username\', \'useremail\', \'myRadioBtnGroup\', \'myCheckboxGroup\');
                            foreach ($required as $required) {
                                $validator->required()->validate($required);
                            }
                            $validator->email()->validate(\'useremail\');
                            $validator->min(7)->max(15)->validate(\'userpass\');
                            // check for errors
                            if ($validator->hasErrors()) {
                                $_SESSION[\'errors\'][\'my-contact-form\'] = $validator->getAllErrors();
                            } else { // if posted values are ok
                                // Do your stuff (send e-mail, ...).
                            }
                        }
                        /* ==================================================
                            for class and methods documentation,
                            go to documentation/index.html
                        ================================================== */
                        $form = new Form(\'my-form-id\');
                        $form->startFieldset(\'My horizontal form\');
                        $form->addInput(\'text\', \'username\', \'\', \'Please enter your name : \', \'id=username-id, placeholder=My Placeholder Text, required=required\');
                        $form->addInput(\'email\', \'useremail\', \'\', \'Please enter your email : \', \'placeholder=Your email here, required=required\');
                        $form->addHtml(\'<p class="help-block">Your password must contain 7 characters min. and and 15 characters max.</p>\', \'userpass\', \'after\');
                        $form->addInput(\'password\', \'userpass\', \'\', \'Please enter your password : \', \'required=required, pattern=(.){7\,15}\');');
                        if (!isset($_GET['vertical'])) {
                        echo htmlspecialchars('
                        $form->addHtml(\'<div class="col-sm-12 alert alert-info">Input grouped on same line with placeholder :</div>\');
                        $form->groupInputs(\'street\', \'zip\', \'country\');
                        $form->setOptions(array(\'horizontalElementCol\' => \'col-sm-3\'));
                        $form->addInput(\'text\', \'street\', \'\', \'Your adress : \', \'placeholder=street,required=required\');
                        $form->setOptions(array(\'horizontalLabelCol\' => \'\', \'horizontalOffsetCol\' => \'\', \'horizontalElementCol\' => \'col-sm-2\'));
                        $form->addInput(\'text\', \'zip\', \'\', \'\', \'placeholder=zip code,required=required\');
                        $form->setOptions(array(\'horizontalElementCol\' => \'col-sm-3\'));
                        $form->addOption(\'country\', \'\', \'Country\');
                        $form->addOption(\'country\', \'United States\', \'United States\');
                        $form->addOption(\'country\', \'Canada\', \'Canada\');
                        $form->addOption(\'country\', \'France\', \'France\');
                        $form->addSelect(\'country\', \'\', \'\');
                        $form->setOptions(array(\'horizontalLabelCol\' => \'col-sm-4\', \'horizontalOffsetCol\' => \'col-sm-4\', \'horizontalElementCol\' => \'col-sm-8\'));');
                        }
                        echo htmlspecialchars('
                        $form->addTextarea(\'my-textarea\', \'Enter your message here ...\', \'Your message : \', \'cols=30, rows=4\');
                        $form->addPlugin(\'word-character-count\', \'#my-textarea\', \'default\', array(\'%maxAuthorized%\' => 100));
                        for ($i=1; $i < 6; $i++) {
                            $form->addOption(\'mySelectName\', $i, \'option \' . $i, \'group 1\');
                        }
                        for ($i=6; $i < 11; $i++) {
                            $form->addOption(\'mySelectName\', $i, \'option \' . $i, \'group 2\');
                        }
                        $form->addSelect(\'mySelectName\', \'Select please : \', \'\');
                        for ($i=1; $i < 11; $i++) {
                            $form->addOption(\'myMultipleSelectName\', $i, \'option \' . $i);
                        }
                        $form->addSelect(\'myMultipleSelectName\', \'Select please :<br>(multiple selections)\', \'multiple=multiple\');
                        $form->addRadio(\'myRadioBtnGroup\', \'choice one\', \'value 1\');
                        $form->addRadio(\'myRadioBtnGroup\', \'choice two\', \'value 2\');
                        $form->addRadio(\'myRadioBtnGroup\', \'choice three\', \'value 3\');
                        if (!isset($_SESSION[\'myRadioBtnGroup\'])) {
                            $_SESSION[\'myRadioBtnGroup\'] = \'value 3\';
                        }
                        $form->printRadioGroup(\'myRadioBtnGroup\', \'Choose one please : \', $inline = true, $attr = \'required=required\');
                        $form->addCheckbox(\'myCheckboxGroup\', \'choice one\', \'choice-1\', \'value 1\');
                        $form->addCheckbox(\'myCheckboxGroup\', \'choice two\', \'choice-2\', \'value 2\');
                        $form->addCheckbox(\'myCheckboxGroup\', \'choice three\', \'choice-3\', \'value 3\');
                        if (!isset($_SESSION[\'myCheckboxGroup\'])) {
                            $_SESSION[\'choice-2\'] = \'value 2\';
                        }
                        $form->printCheckboxGroup(\'myCheckboxGroup\', \'Check please : \', $inline = true, $attr = \'required=required\');
                        $form->addInputWrapper(\'<div class="row"><div class="col-lg-4"></div></div>\', \'myInputLg4\');
                        $form->addInput(\'text\', \'myInputLg4\', \'\', \'input width : \');
                        $form->addInputWrapper(\'<div class="input-group"></div>\', \'inputPrepend\');
                        $form->addHtml(\'<div class="input-group-addon">@</div>\', \'inputPrepend\', \'before\');
                        $form->addInput(\'text\', \'inputPrepend\', \'\', \'prepended text : \');
                        $form->addInputWrapper(\'<div class="input-group"></div>\', \'inputAppend\');
                        $form->addHtml(\'<div class="input-group-addon">€</div>\', \'inputAppend\', \'after\');
                        $form->addInput(\'text\', \'inputAppend\', \'\', \'appended text : \');
                        $form->endFieldset();
                        $form->startFieldset(\'Plugins\');
                        $form->addInput(\'text\', \'defaultColorPicker\', \'\', \'ColorPicker : \');
                        $form->addPlugin(\'colorpicker\', \'#defaultColorPicker\');
                        $form->addInput(\'text\', \'myCustomColorPicker\', \'\', \'custom ColorPicker : \');
                        $myCustomXmlReplacement = array(\'%css-property%\' => \'color\');
                        $form->addPlugin(\'colorpicker\', \'#myCustomColorPicker\', \'custom\', $myCustomXmlReplacement);
                        $form->addInput(\'text\', \'myTimePicker\', \'\', \'TimePicker : \');
                        $form->addPlugin(\'timepicker\', \'#myTimePicker\');
                        $form->addInput(\'text\', \'myDatePicker\', \'\', \'DatePicker : \');
                        $form->addPlugin(\'datepicker\', \'#myDatePicker\');
                        $fileUpload_config = array(
                            \'xml\'                 => \'images\',
                            \'uploader\'            => \'imageFileUpload.php\',
                            \'btn-text\'            => \'Browse ...\',
                            \'max-number-of-files\' => 2
                        );
                        $form->addFileUpload(\'file\', \'files[]\', \'\', \'FileUpload : <br><p class="help-block">Upload for images - 2 images max.</p>\', \'id=myFileUpload\', $fileUpload_config);
                        $form->addInput(\'text\', \'captcha\', \'\', \'Type the characters please :\', \'size=15\');
                        $form->addPlugin(\'captcha\', \'#captcha\', \'custom\');
                        $form->addBtn(\'button\', \'myBtnName\', 1, \'Button alone\', \'class=btn btn-primary\');
                        $form->addBtn(\'button\', \'myBtnCancelName\', 1, \'Cancel\', \'class=btn btn-danger, onclick=alert(\\\'cancelled\\\');\', \'myBtnGroup\');
                        $form->addBtn(\'submit\', \'myBtnSubmitName\', 1, \'Submit\', \'class=btn btn-success\', \'myBtnGroup\');
                        $form->printBtnGroup(\'myBtnGroup\');
                        $form->endFieldset();
                        ?>
                        <html>
                        <head>
                            [...]
                            <?php $form->printIncludes(\'css\'); ?>
                        </head>
                        <body>
                            [...]
                            <?php $form->render(); ?>
                            [...]
                            <?php
                                $form->printIncludes(\'js\');
                                $form->printJsCode();
                            ?>
                        </body>
                        </html>'); ?></pre>
                      </div>
                      <div class="tab-pane<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") echo ' active'; ?>" id="result">
                        <?php $form->render(); ?>
                      </div><div class="tab-pane" id="html">
                        <p><span class="label label-default">&lt;head&gt; section : </span></p>
                        <pre class="prettyprint"><?php echo htmlspecialchars('<link href="http://codecanyon.creation-site.org/phpforms/plugins/colpick-jQuery-Color-Picker-master/css/colpick.css" rel="stylesheet" media="screen">
<link href="http://codecanyon.creation-site.org/phpforms/plugins/jquery-timepicker-master/jquery.timepicker.css" rel="stylesheet" media="screen">
<link href="http://codecanyon.creation-site.org/phpforms/plugins/jquery-ui/smoothness/jquery-ui.min.css" rel="stylesheet" media="screen">
<link href="http://codecanyon.creation-site.org/phpforms/plugins/jQuery-File-Upload-9.5.8/css/jquery.fileupload.css" rel="stylesheet" media="screen">
<link href="http://codecanyon.creation-site.org/phpforms/plugins/jquery-realperson/jquery.realperson.css" rel="stylesheet" media="screen">'); ?></pre>
                        <p><span class="label label-default">The form : </span></p>
                        <?php $form->render(true); ?>
                        <p><span class="label label-default">JS : </span></p>
                        <pre class="prettyprint"><?php echo htmlspecialchars('<script src="http://codecanyon.creation-site.org/phpforms/plugins/colpick-jQuery-Color-Picker-master/js/colpick.js"></script>
<script src="http://codecanyon.creation-site.org/phpforms/plugins/jquery-timepicker-master/jquery.timepicker.min.js"></script>
<script src="http://codecanyon.creation-site.org/phpforms/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="http://codecanyon.creation-site.org/phpforms/plugins/jQuery-File-Upload-9.5.8/js/vendor/jquery.ui.widget.js"></script>
<script src="http://codecanyon.creation-site.org/phpforms/plugins/jQuery-File-Upload-9.5.8/js/jquery.iframe-transport.js"></script>
<script src="http://codecanyon.creation-site.org/phpforms/plugins/jQuery-File-Upload-9.5.8/js/jquery.fileupload.js"></script>
<script src="http://codecanyon.creation-site.org/phpforms/plugins/jQuery-File-Upload-9.5.8/js/tmpl.min.js"></script>
<script src="http://codecanyon.creation-site.org/phpforms/plugins/jQuery-File-Upload-9.5.8/js/load-image.min.js"></script>
<script src="http://codecanyon.creation-site.org/phpforms/plugins/jQuery-File-Upload-9.5.8/js/canvas-to-blob.min.js"></script>
<script src="http://codecanyon.creation-site.org/phpforms/plugins/jQuery-File-Upload-9.5.8/js/jquery.fileupload-process.js"></script>
<script src="http://codecanyon.creation-site.org/phpforms/plugins/jQuery-File-Upload-9.5.8/js/jquery.fileupload-image.js"></script>
<script src="http://codecanyon.creation-site.org/phpforms/plugins/jQuery-File-Upload-9.5.8/js/jquery.fileupload-validate.js"></script>
<script src="http://codecanyon.creation-site.org/phpforms/plugins/jQuery-File-Upload-9.5.8/js/jquery.fileupload-ui.js"></script>
<script src="http://codecanyon.creation-site.org/phpforms/plugins/jquery-realperson/jquery.plugin.min.js"></script>
<script src="http://codecanyon.creation-site.org/phpforms/plugins/jquery-realperson/jquery.realperson.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#defaultColorPicker").colpick({
            onSubmit:function (hsb,hex,rgb,el) {
                $(el).val(\'#\'+hex);
                $(el).colpickHide();
            }
        });
        $("#myCustomColorPicker").colpick({
            colorScheme:\'dark\',
            layout:\'rgbhex\',
            color:\'ff8800\',
            onSubmit:function (hsb,hex,rgb,el) {
                $(el).val(\'you choosed #\'+hex).css(\'color\', \'#\'+hex);
                $(el).colpickHide();
            }
        });
        $("#myTimePicker").timepicker();
        $("#myDatePicker").datepicker();
        $("#captcha").realperson({
            length: 6,
            regenerate:\'Click to change\',
            dot: \'#\'
        });
    });
</script>
[ + All JS code for file upload, not displayed in this demo.]'); ?></pre>
                      </div>
                </div>
            </div>
            </div>
        </div>
        <!-- jQuery -->
        <script src="//code.jquery.com/jquery.js"></script>
        <!-- Bootstrap JavaScript -->
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
        <!-- Demo prettify code -->
        <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js"></script>
        <?php
            $form->printIncludes('js');
            $form->printJsCode();
        ?>
    </body>
</html>
