<?php session_start();

/* =============================================
    start session and include form class
============================================= */

use phpforms\Form;
use phpforms\Validator\Validator;

include_once '../Form.php';

/* =============================================
    validation if posted
============================================= */

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    include_once '../Validator/Validator.php';
    include_once '../Validator/Exception.php';
    $validator = new Validator($_POST);
    $required = array('username', 'useremail', 'userphone', 'message');
    foreach ($required as $required) {
        $validator->required()->validate($required);
    }
    $validator->email()->validate('useremail');
    $validator->captcha('captcha')->validate('captcha');
    // check for errors
    if ($validator->hasErrors()) {
        $_SESSION['errors']['my-contact-form'] = $validator->getAllErrors();
    } else { // if posted values are ok
        echo '<h2>ALL OK</h2>';
    }
}

/* ==================================================
    for class and methods documentation,
    go to documentation/index.html
================================================== */

$form = new Form('my-contact-form');
$form->startFieldset('Please fill in this form to contact us');
$form->addInput('text', 'username', '', 'Your name : ', 'required=required');
$form->addInput('email', 'useremail', '', 'Your email : ', 'required=required');
$form->addInput('text', 'userphone', '', 'Your phone : ', 'required=required');
$form->addOption('subject', 'Support', 'Support');
$form->addOption('subject', 'Sales', 'Sales');
$form->addOption('subject', 'Other', 'Other');
$form->addSelect('subject', 'Subject : ');
$form->addTextarea('content-word-char-count', '', 'tinymce : ', 'cols=100, rows=20, class=tinyMce');
$form->addPlugin('tinymce', '#content-word-char-count', 'word_char_count', array('%maxAuthorized%' => 200));
$_SESSION['newsletter'] = 1; // to add default-check.
$form->addCheckbox('newsletter-checkboxes', '', 'newsletter', 1);
$form->printCheckboxGroup('newsletter-checkboxes', 'Suscribe to Newsletter :');
$form->addInput('text', 'captcha', '', 'Type the characters please :', 'size=15');
$form->addPlugin('captcha', '#captcha');
$form->addBtn('submit', 'submit-btn', 1, 'Submit', 'class=btn btn-success');
$form->endFieldset();
?>

<html>
<head>
    <title>test</title>
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
    <?php $form->printIncludes('css'); ?>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <?php $form->render(); ?>
            </div>
        </div>
    </div>
    <script src="http://code.jquery.com/jquery.js"></script>
    <?php
        $form->printIncludes('js');
        $form->printJsCode();
    ?>
</body>
</html>
