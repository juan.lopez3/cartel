(function() {


    //Section 1 : Code to execute when the toolbar button is pressed
    var a = {
        exec: function(editor, element, useComputedState ) {



            //consigue el texto seleccionado
            var selected_text = editor.getSelection().getSelectedText(); // Get Text
            //valida si tiene el estilo si lo tiene se lo quita si no se lo pone
            if (editor.getSelection().getStartElement().hasAttribute('style')) {

                

                if (editor.getSelection().getStartElement().getAttribute('style') == 'color:#00aeef !important; font-size:25px !important;font-family: "bebas_neueregular",Arial-narrow !important;') {
                    //remove style attribute
                    editor.getSelection().getStartElement().removeAttribute('style');
                }
            }else{
            var newElement = new CKEDITOR.dom.element("p");              // Make Paragra
            newElement.setAttributes({style: 'color:#00aeef !important; font-size:25px !important; font-family: "bebas_neueregular",Arial-narrow !important;'})                 // Set Attributess
            newElement.setText(selected_text);                           // Set text to element
            editor.insertElement(newElement);         
        }

    }
},
   


    //Section 2 : Create the button and add the functionality to it
    b='bocadillos';
    CKEDITOR.plugins.add(b, {
        init: function(editor) {
            editor.addCommand(b, a);
            editor.ui.addButton("bocadillos", {
                label: 'Bocadillos', 
                icon: "plugins/bocadillos/images/t.png",
                command: b
            });
        }
    }); 

})();
