(function() {

	// Section 1 : Code to execute when the toolbar button is pressed
	var a = {
		exec : function(editor, element, useComputedState) {
			editor.insertHtml('<p><!--ad_display--></p>');
		}
	},

	// Section 2 : Create the button and add the functionality to it
	b = 'ad_banner_instant_articles';
	CKEDITOR.plugins.add(b, {
		init : function(editor) {
			editor.addCommand(b, a);
			editor.ui.addButton("ad_banner_instant_articles", {
				label : 'Banner en instant articles',
				icon : "plugins/ad_banner_instant_articles/images/ad-ads-advertisement-advertising-banner-frame-promotion-icon-5.png",
				command : b
			});
		}
	});

})();
