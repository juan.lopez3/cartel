(function() {


    //Section 1 : Code to execute when the toolbar button is pressed
    var a = {
        exec: function(editor, element, useComputedState ) {



            //consigue el texto seleccionado
            var selected_text = editor.getSelection().getSelectedText(); // Get Text
            //valida si tiene el estilo si lo tiene se lo quita si no se lo pone
            if (editor.getSelection().getStartElement().hasAttribute('style')) {
                if (editor.getSelection().getStartElement().getAttribute('style') == 'font-size:21px !important;') {
                    editor.getSelection().getStartElement().removeStyle( 'font-size' );
                }
            }else{
            var newElement = new CKEDITOR.dom.element("p");              // Make Paragraff
            newElement.setAttributes({style: 'font-size:21px !important;'})                 // Set Attributes
            newElement.setText(selected_text);                           // Set text to element
            editor.insertElement(newElement);         
        }

    }
},



    //Section 2 : Create the button and add the functionality to it
    b='subtitulos';
    CKEDITOR.plugins.add(b, {
        init: function(editor) {
            editor.addCommand(b, a);
            editor.ui.addButton("subtitulos", {
                label: 'sub titulos', 
                icon: "plugins/subtitulos/images/st.png",
                command: b
            });
        }
    }); 

})();
