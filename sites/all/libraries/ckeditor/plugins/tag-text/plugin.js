(function() {


    //Section 1 : Code to execute when the toolbar button is pressed
    var a = {
        exec: function(editor, element, useComputedState ) {



            //consigue el texto seleccionado
            var selected_text = editor.getSelection().getSelectedText(); // Get Text
            //valida si tiene el estilo si lo tiene se lo quita si no se lo pone
            if (editor.getSelection().getStartElement().hasAttribute('style')) {

                

                if (editor.getSelection().getStartElement().getAttribute('style') == ' margin-top: -30px !important; font-size:13px !important;') {
                    //remove style attribute
                    editor.getSelection().getStartElement().removeAttribute('style');
                }
            }else{
            var newElement = new CKEDITOR.dom.element("p");              // Make Paragra
            newElement.setAttributes({style: ' margin-top: -30px !important; font-size:13px !important;'})                 // Set Attributess
            newElement.setText(selected_text);                           // Set text to element
            editor.insertElement(newElement);         
        }

    }
},
   


    //Section 2 : Create the button and add the functionality to it
    b='tag-text';
    CKEDITOR.plugins.add(b, {
        init: function(editor) {
            editor.addCommand(b, a);
            editor.ui.addButton("tag-text", {
                label: 'pie de foto', 
                icon: "plugins/tag-text/images/t.png",
                command: b
            });
        }
    }); 

})();
