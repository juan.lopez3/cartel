<?php 
// Ignore the data in these tables, but keep the table structures. Make sure to 
// update the list when new cache tables are added. 
//- See more at: http://www.mediacurrent.com/blog/exporting-and-reloading-drupal-databases-drush#sthash.xyQQCzUm.dpuf

// Add tables to structure-only list
// Set default if it doesn't exist. Copied from example.drushrc.php
if (!isset($options['structure-tables']['common'])) {
  $options['structure-tables']['common'] = array('cache', 'cache_filter', 'cache_menu', 'cache_page', 'history', 'sessions', 'watchdog');
}
 

$tablas = array( 
		'cache', 'cache_admin_menu', 'cache_apachesolr', 'cache_block', 'cache_bootstrap', 
		'cache_field', 'cache_filter', 'cache_form', 'cache_image', 'cache_libraries', 'cache_media_xml', 'cache_menu', 
		'cache_page', 'cache_path', 'cache_token', 'cache_update', 'cache_views', 'cache_views_data', 
		'history', 'search_dataset', 'search_index', 'search_node_links', 'search_total', 'sessions', 'watchdog'); 
		
$options['structure-tables']['common'] = array_merge($options['structure-tables']['common'], $tablas);


		
		// Use the list of cache tables above. 
$options['structure-tables-key'] = 'common'; 

?> 
