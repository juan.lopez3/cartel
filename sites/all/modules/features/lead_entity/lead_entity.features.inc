<?php
/**
 * @file
 * lead_entity.features.inc
 */

/**
 * Implements hook_eck_entity_type_info().
 */
function lead_entity_eck_entity_type_info() {
$items = array(
       'lead' => array(
  'name' => 'lead',
  'label' => 'Lead',
  'properties' => array(
    'created' => array(
      'label' => 'Created',
      'type' => 'integer',
      'behavior' => 'created',
    ),
    'changed' => array(
      'label' => 'Changed',
      'type' => 'integer',
      'behavior' => 'changed',
    ),
    'language' => array(
      'label' => 'Entity language',
      'type' => 'language',
      'behavior' => 'language',
    ),
  ),
),
  );
  return $items;
}
