(function ($) {
  Drupal.behaviors.mymodule = {
    attach: function(context, settings) {
      function updateData(email) {
        var email_input = email;
        jQuery.ajax({
            method: "POST",
            url: "/leads/email_not_exist/",
            data: { email: $('input.lead-email-field').val() }
          }).done(function( data ) {
            if(data.response == 'ok') {

              jQuery('.lead-fields').show();
              jQuery('.lead-name-field').focus();

            } else if(data.response == 'incomplete') {
               jQuery.each(data.fields_info, function(key, value){
                 if(value) {
                   jQuery('.'+key).show();
                 } else {
                   jQuery('.'+key).hide();
                 }
               }); 
            }

            else{
              jQuery('.lead-fields').hide();
            }

          }).error(function(){
            jQuery('.lead-fields').hide();
          });
      }
      $('input.lead-email-field').focusout(function(){
        var email_input = jQuery(this);
        updateData(email_input);
      });
      if($('input.lead-email-field').val() != '') {
        updateData($('input.lead-email-field').val());
      }
      
  }
}})(jQuery);
