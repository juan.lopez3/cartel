<?php
/**
 * @file
 * Implementation of hook_panels_layouts
 */
function cartel_block_one_66_panels_layouts() {
  $items['block_one_66'] = array(
    'title'    => t('Bloque muestra 2 columnas'),
    'category' => t('bloque responsive - 1 col 66'),
    'icon'     => 'block_one_66.png',
    'theme'    => 'block_one_66',
    'admin css' => 'block_one_66.admin.css',
    'regions' => array(
      'one_main'  => t('Principal')
    )
  );
  return $items;
}