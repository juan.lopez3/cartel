<?php
/**
 * @file
 * Panels layout una columna.
 */
?>
<!-- container fit -->
<div class="cartel-block col-xs-12 col-sm-4">
	<div class="<?php print $classes; ?>" <?php print $attributes; ?>>
		<?php print render($title_prefix); ?>
		<?php print $content['one_main']; ?>
		<?php print render($title_suffix); ?>
	</div>
</div>