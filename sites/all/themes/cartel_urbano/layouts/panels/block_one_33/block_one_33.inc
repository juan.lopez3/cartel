<?php
/**
 * @file
 * Implementation of hook_panels_layouts
 */
function cartel_block_one_33_panels_layouts() {
  $items['block_one_33'] = array(
    'title'    => t('Bloque muestra 1 columna'),
    'category' => t('bloque responsive - 1 col 33'),
    'icon'     => 'block_one_33.png',
    'theme'    => 'block_one_33',
    'admin css' => 'block_one_33.admin.css',
    'regions' => array(
      'one_main'  => t('Principal')
    )
  );
  return $items;
}