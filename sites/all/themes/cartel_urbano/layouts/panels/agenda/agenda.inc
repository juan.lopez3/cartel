<?php
/**
 * @file
 * Implementation of hook_panels_layouts
 */
function cartel_agenda_panels_layouts() {
  $items['agenda'] = array(
    'title'    => t('Agenda'),
    'category' => t('Cartel Panels - agenda'),
    'icon'     => 'agenda.png',
    'theme'    => 'agenda',
    'admin css' => 'agenda.admin.css',
    'regions' => array(
      'agenda_top' => t('Top (slider, mas popular)'),
      'agenda_breadcrumb' => t('Miga de pan'),
      'agenda_main' => t('Principal'),
      'agenda_bottom' => t('Bottom (opcional)')
    )
  );
  return $items;
}