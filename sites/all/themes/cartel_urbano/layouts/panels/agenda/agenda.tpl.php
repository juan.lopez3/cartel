<?php
/**
 * @file
 * Panels layout para agenda.
 */
?>
<!-- contenedor fluido -->
<div class="agenda-tpl agenda-top cartel-panel panel-display" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
  <div class="region row">
    <div class="region-top region-inner col-sm-12 col-fluid">
      <?php print $content['agenda_top']; ?>
    </div>
  </div>
</div>

<!-- Breadcrumb -->
<div class="agenda-tpl cartel-panel panel-display agenda-breadcrumb" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
  <div class="region row">
    <div class="region-main region-inner col-sm-10 col-sm-offset-1">
      <?php print $content['agenda_breadcrumb']; ?>
    </div>
  </div>
</div>

<!-- Listado eventos -->
<div class="agenda-tpl cartel-panel panel-display agenda-eventos" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
  <div class="region row">
    <div class="region-main region-inner col-sm-12 col-fluid">
      <div id="loading" class="cargando"></div>
      <?php print $content['agenda_main']; ?>
    </div>
  </div>
</div>

<!-- contenedor fluido -->
<div class="agenda-tpl agenda-bottom cartel-panel panel-display" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
  <div class="region row">
    <div class="region-bottom region-inner col-sm-12 col-fluid">
      <?php print $content['agenda_bottom']; ?>
    </div>
  </div>
</div>