<?php
/**
 * @file
 * Implementation of hook_panels_layouts
 */
function cartel_one_panels_layouts() {
  $items['one'] = array(
    'title'    => t('cartel una columna'),
    'category' => t('Cartel Panels - 1 columna'),
    'icon'     => 'one.png',
    'theme'    => 'one',
    'admin css' => 'one.admin.css',
    'regions' => array(
      'one_top' => t('Top (opcional)'),
      'one_main'  => t('Principal'),
      'one_bottom' => t('Bottom (opcional)')
    )
  );
  return $items;
}