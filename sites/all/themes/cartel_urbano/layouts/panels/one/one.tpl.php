<?php
/**
 * @file
 * Panels layout una columna.
 */
?>
<!-- contenedor fluido -->
<div class="one-column-top cartel-panel panel-display" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
  <div class="region row">
    <div class="region-top region-inner col-sm-12 col-fluid">
      <?php print $content['one_top']; ?>
    </div>
  </div>
</div>

<!-- contenedor ajustado -->
<div class="one-column-main cartel-panel panel-display" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
  <div class="region row">
    <div class="region-main region-inner col-sm-10 col-sm-offset-1">
      <div id="loading" class="cargando"></div>
      <?php print $content['one_main']; ?>
    </div>
  </div>
</div>

<!-- contenedor fluido -->
<div class="one-column-bottom cartel-panel panel-display" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
  <div class="region row">
    <div class="region-bottom region-inner col-sm-12 col-fluid">
      <?php print $content['one_bottom']; ?>
    </div>
  </div>
</div>