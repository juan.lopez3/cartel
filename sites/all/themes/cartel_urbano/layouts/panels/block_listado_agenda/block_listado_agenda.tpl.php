<?php
/**
 * @file
 * Bloque layout 4 columnas.
 */
?>
<!-- container fit -->
<div class="cartel-block col-xs-6 col-sm-3 cartel-block-agenda">
	<div class="<?php print $classes; ?>" <?php print $attributes; ?>>
		<?php print render($title_prefix); ?>
		<?php print $content['one_main']; ?>
		<?php print render($title_suffix); ?>
	</div>
</div>