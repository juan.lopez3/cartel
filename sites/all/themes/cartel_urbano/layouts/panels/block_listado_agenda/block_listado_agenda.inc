<?php
/**
 * @file
 * Implementation of hook_panels_layouts
 */
function cartel_block_listado_agenda_panels_layouts() {
  $items['block_listado_agenda'] = array(
    'title'    => t('Bloque listado cartel'),
    'category' => t('bloque responsive - 25%'),
    'icon'     => 'block_listado_agenda.png',
    'theme'    => 'block_listado_agenda',
    'admin css' => 'block_listado_agenda.admin.css',
    'regions' => array(
      'one_main'  => t('Principal')
    )
  );
  return $items;
}