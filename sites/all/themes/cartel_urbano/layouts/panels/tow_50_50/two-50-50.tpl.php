<?php
/**
 * @file
 * Panels layout una columna.
 */
?>
<!-- contenedor fluido -->
<div class="two-50-50-column cartel-panel panel-display" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
  <div class="region row">
    <div class="region-top region-inner col-sm-12 col-fluid">
      <?php print $content['col_top']; ?>
    </div>
  </div>
</div>

<!-- contenedor ajustado -->
<div class="two-50-50-column cartel-panel panel-display">
  <div class="region row">

  	<!-- col left -->
    <div class="region-left region-inner col-sm-5 col-sm-offset-1" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
      <?php print $content['col_left']; ?>
    </div>

    <!-- col right -->
    <div class="region-right region-inner col-sm-5" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
      <?php print $content['col_right']; ?>
    </div>

  </div>
</div>

<!-- contenedor fluido -->
<div class="two-50-50-column cartel-panel panel-display" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
  <div class="region row">
    <div class="region-bottom region-inner col-sm-12 col-fluid">
      <?php print $content['col_bottom']; ?>
    </div>
  </div>
</div>