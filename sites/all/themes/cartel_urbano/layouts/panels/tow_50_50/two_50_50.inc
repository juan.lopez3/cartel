<?php
/**
 * @file
 * Implementation of hook_panels_layouts
 */
function cartel_two_50_50_panels_layouts() {
  $items['two_50_50'] = array(
    'title'    => t('cartel dos columnas 50 50'),
    'category' => t('Cartel Panels - 2 columnas 50 50'),
    'icon'     => 'two_50_50.png',
    'theme'    => 'two-50-50',
    'admin css' => 'two_50_50.admin.css',
    'regions' => array(
      'col_top'  => t('Arriba'),
      'col_left' => t('Izquierda'),
      'col_right' => t('Derecha'),
      'col_bottom' => t('Abajo')
    )
  );
  return $items;
}