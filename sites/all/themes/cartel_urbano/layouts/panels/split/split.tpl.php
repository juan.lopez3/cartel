<?php
/**
 * @file
 * Panels layout una columna.
 */
?>
<div class="layout-split layout-split-top cartel-panel panel-display" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
  <div class="region row">
    <?php print $content['top']; ?>
  </div>
</div>

<div class="layout-split cartel-panel panel-display">
  <div class="region row">

  	<!-- col left -->
    <div class="region-left region-inner col-sm-3 col-sm-offset-1" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
      <?php print $content['col_left']; ?>
    </div>

    <!-- col right -->
    <div class="region-right region-inner col-sm-8" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
      <?php print $content['col_right']; ?>
    </div>

  </div>
</div>