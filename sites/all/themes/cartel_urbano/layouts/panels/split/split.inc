<?php
/**
 * @file
 * Implementation of hook_panels_layouts
 */
function cartel_split_panels_layouts() {
  $items['split'] = array(
    'title'    => t('cartel split'),
    'category' => t('Cartel Panels - split'),
    'icon'     => 'split.png',
    'theme'    => 'split',
    'admin css' => 'split.admin.css',
    'regions' => array(
      'top' => t('Top'),
      'col_left' => t('Izquierda'),
      'col_right' => t('Derecha')
    )
  );
  return $items;
}