<?php
/**
 * @file
 * Implementation of hook_panels_layouts
 */
function cartel_two_8_4_panels_layouts() {
  $items['two_8_4'] = array(
    'title'    => t('cartel dos columnas 66 33'),
    'category' => t('Cartel Panels - 2 columnas 66 33'),
    'icon'     => 'two_8_4.png',
    'theme'    => 'two-8-4',
    'admin css' => 'two_8_4.admin.css',
    'regions' => array(
      'col_top'  => t('Arriba'),
      'col_left' => t('Izquierda'),
      'col_right' => t('Derecha'),
      'col_bottom' => t('Abajo')
    )
  );
  return $items;
}