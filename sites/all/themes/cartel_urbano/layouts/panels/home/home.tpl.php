<?php
/**
 * @file
 * Panels layout home.
 */
?>
<!-- contenedor fluido(Destacado) -->
<div class="home-panel cartel-panel panel-display" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
  <div class="region row">
    <div class="region-destacado region-inner col-sm-12 col-fluid">
      <?php print $content['home_destacado']; ?>
    </div>
  </div>
</div>

<!-- contenedor fluido(publicidad) -->
<div class="home-panel cartel-panel panel-display" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
  <div class="region row">
    <div class="region-publicidad region-inner col-sm-12 col-fluid">
      <?php print $content['home_publicidad']; ?>
    </div>
  </div>
</div>

<!-- contenedor ajustado(Agenda, galerias) -->
<div class="home-panel cartel-panel panel-display">
  <div class="region row">

  	<!-- col left -->
    <div class="region-agenda region-inner col-sm-5 col-sm-offset-1" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
      <?php print $content['home_agenda']; ?>
    </div>

    <!-- col right -->
    <div class="region-galerias region-inner col-sm-5" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
      <?php print $content['home_galerias']; ?>
    </div>

  </div>
</div>

<!-- contenedor fluido(Secciones, Cartel tv) -->
<div class="home-panel cartel-panel panel-display" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
  <div class="region row">
    <div class="region-secciones region-inner col-sm-12 col-fluid">
      <?php print $content['home_secciones']; ?>
    </div>
  </div>
</div>

<!-- contenedor fluido(para poner fondo fullwidth) -->
<div class="home-panel cartel-panel panel-display">
  <div class="opinion row" data-fondo="url-img">
    <div class="col-sm-12">

      <!-- contenedor Ajustado(opinion) -->
      <div class="row">
        <div class="region-opinion region-inner col-sm-10 col-sm-offset-1">
          <?php print $content['home_opinion']; ?>
        </div>
      </div>
      
    </div>
  </div>
</div>

<!-- contenedor ajustado(Fullgente) -->
<div class="home-panel cartel-panel panel-display">
  <div class="region row">

    <div class="region-fullgente region-inner col-sm-10 col-sm-offset-1" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
      <?php print $content['home_fullgente']; ?>
    </div>

  </div>
</div>