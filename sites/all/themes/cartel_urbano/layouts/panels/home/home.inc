<?php
/**
 * @file
 * Implementation of hook_panels_layouts
 */
function cartel_home_panels_layouts() {
  $items['home'] = array(
    'title'    => t('cartel home'),
    'category' => t('Cartel Panels - home'),
    'icon'     => 'home.png',
    'theme'    => 'home',
    'admin css' => 'home.admin.css',
    'regions' => array(
      'home_destacado'  => t('Destacado'),
      'home_publicidad'  => t('Publicidad'),
      'home_agenda' => t('Agenda'),
      'home_galerias' => t('Galerias'),
      'home_secciones' => t('Secciones'),
      'home_opinion' => t('Opinion'),
      'home_fullgente' => t('fullgente')
    )
  );
  return $items;
}