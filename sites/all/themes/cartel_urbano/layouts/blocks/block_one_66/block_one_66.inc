<?php
/**
 * @file
 * Implementation of hook_panels_layouts
 */
function cartel_block_one_66_blocks_layouts() {
  $items['block_one_66'] = array(
    'title'    => t('cartel una columna'),
    'category' => t('block responsive - 1 col 66'),
    'icon'     => 'block_one_66.png',
    'theme'    => 'block_one_66',
    'admin css' => 'block_one_66.admin.css',
    'regions' => array(
      'one_main'  => t('Principal')
    )
  );
  return $items;
}