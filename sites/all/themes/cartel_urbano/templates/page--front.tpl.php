<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * Regions:
 * - $page['header']: item en el header - Logo, tag, buscador, redes, ver por, Ed. impresa.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['header-seccion']: entradilla de seccion.
 * - $page['content']: Contenido principal de la pagina actual.
 * - $page['realcionado']: Contenido relacionado.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>

<!-- #banner_pop_up -->	
<?php print render($page['banner_pop_up']); ?>
<!-- /#banner_pop_up -->
<!-- #banner_welcome_mat -->	
<?php print render($page['banner_welcome_mat']); ?>
<!-- /#banner_pop_up -->

<div class="main-container container-fluid">

  <header role="banner" id="page-header-seccion">
	<!-- #banner_header -->	
	<?php print render($page['banner_header']); ?>
	<!-- /#banner_header -->
    <?php print render($page['header-seccion']); ?>
    
  </header> <!-- /#page-header-seccion -->
  
  <div class="row">
    
    <?php if ($messages):?>
      <div id="mensajes">
        <?php print $messages;?>
      </div>
    <?php endif;?>
    
    <?php if (!empty($page['breadcrumbs'])): ?>
      <section class="col-sm-12 breadcrumbs">
        <?php if (!empty($breadcrumb)): print $breadcrumb; endif;?>
      </section>
    <?php endif; ?>

    <section<?php print $content_column_class; ?>>
      <!-- admin tabs -->
      <?php if (!empty($tabs)): ?>
        <?php print render($tabs); ?>
      <?php endif; ?><!-- admin tabs -->

      <!-- admin help -->
      <?php if (!empty($page['help'])): ?>
        <?php print render($page['help']); ?>
      <?php endif; ?><!-- admin help -->

      <?php if (!empty($action_links)): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>

      <!-- PAGE CONTENT -->
      <?php print render($page['content']); ?>
    </section>

    <?php if (!empty($page['sidebar_first'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_first']); ?>
      </aside>  <!-- /#sidebar-second -->
    <?php endif; ?>


  </div>
</div><!-- main-container -->

<footer class="footer container-fluid">
  <?php print render($page['footer']); ?>
</footer>

<!-- #banner_sticky_bar -->	
<?php print render($page['banner_sticky_bar']); ?>
<!-- /#bbanner_sticky_bar -->
<!-- #banner_sticky_bar_desplegable -->	
<?php print render($page['banner_sticky_bar_desplegable']); ?>
<!-- /#banner_sticky_bar_desplegable -->
