<?php
/**
 * @file
 * Default theme implementation to display the basic html structure of a single
 * Drupal page.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or
 *   'rtl'.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE
 *   tag.
 * - $head_title_array: (array) An associative array containing the string parts
 *   that were used to generate the $head_title variable, already prepared to be
 *   output as TITLE tag. The key/value pairs may contain one or more of the
 *   following, depending on conditions:
 *   - title: The title of the current page, if any.
 *   - name: The name of the site.
 *   - slogan: The slogan of the site, if any, and if there is no title.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 *
 * @see bootstrap_preprocess_html()
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 *
 * @ingroup themeable
 */
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
        "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces; ?>>
<head profile="<?php print $grddl_profile; ?>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php print $head; ?>
    <title><?php print $head_title; ?></title>
	<?php print $styles; ?>
    <!-- HTML5 element support for IE6-8 -->
    <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq)return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window,
            document, 'script', '//connect.facebook.net/en_US/fbevents.js');

        fbq('init', '888283061221195');
        fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=888283061221195&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->

	<?php print $scripts; ?>

    <!-- script publicidad -->
    <script language="JavaScript" type="text/javascript"><!--
        var eplDoc = document;
        var eplLL = false;
        var eS1 = 'us.img.e-planning.net';
        var eplArgs = {
            iIF: 1,
            sV: schemeLocal() + "://ads.us.e-planning.net/",
            vV: "4",
            sI: "13b7e",
            sec: "home",
            eIs: ["right_banner2", "right_banner1", "ExpansiblePushVariable", "right_banner3", "ExpanidiblePush", "Slider"]
        };
        function eplCheckStart() {
            if (document.epl) {
                var e = document.epl;
                if (e.eplReady()) {
                    return true;
                } else {
                    e.eplInit(eplArgs);
                    if (eplArgs.custom) {
                        for (var s in eplArgs.custom) {
                            document.epl.setCustomAdShow(s, eplArgs.custom[s]);
                        }
                    }
                    return e.eplReady();
                }
            } else {
                if (eplLL) return false;
                if (!document.body) return false;
                var eS2;
                var dc = document.cookie;
                var cookieName = ('https' === schemeLocal() ? 'EPLSERVER_S' : 'EPLSERVER') + '=';
                var ci = dc.indexOf(cookieName);
                if (ci != -1) {
                    ci += cookieName.length;
                    var ce = dc.indexOf(';', ci);
                    if (ce == -1) ce = dc.length;
                    eS2 = dc.substring(ci, ce);
                }
                var eIF = document.createElement('IFRAME');
                eIF.src = 'about:blank';
                eIF.id = 'epl4iframe';
                eIF.name = 'epl4iframe';
                eIF.width = 0;
                eIF.height = 0;
                eIF.style.width = '0px';
                eIF.style.height = '0px';
                eIF.style.display = 'none';
                document.body.appendChild(eIF);

                var eIFD = eIF.contentDocument ? eIF.contentDocument : eIF.document;
                eIFD.open();
                eIFD.write('<html><head><title>e-planning</title></head><bo' + 'dy></bo' + 'dy></html>');
                eIFD.close();
                var s = eIFD.createElement('SCRIPT');
                s.src = schemeLocal() + '://' + (eS2 ? eS2 : eS1) + '/layers/epl-41.js';
                eIFD.body.appendChild(s);
                if (!eS2) {
                    var ss = eIFD.createElement('SCRIPT');
                    ss.src = schemeLocal() + '://ads.us.e-planning.net/egc/4/13b2d';
                    eIFD.body.appendChild(ss);
                }
                eplLL = true;
                return false;
            }
        }
        eplCheckStart();
        function eplSetAdM(eID, custF) {
            if (eplCheckStart()) {
                if (custF) {
                    document.epl.setCustomAdShow(eID, eplArgs.custom[eID]);
                }
                document.epl.showSpace(eID);
            } else {
                var efu = 'eplSetAdM("' + eID + '", ' + (custF ? 'true' : 'false') + ');';
                setTimeout(efu, 250);
            }
        }
        function eplAD4M(eID, custF) {
            document.write('<div id="eplAdDiv' + eID + '"></div>');
            if (custF) {
                if (!eplArgs.custom) {
                    eplArgs.custom = {};
                }
                eplArgs.custom[eID] = custF;
            }
            eplSetAdM(eID, custF ? true : false);
        }
        function schemeLocal() {
            if (document.location.protocol) {
                protocol = document.location.protocol;
            } else {
                protocol = window.top.location.protocol;
            }
            if (protocol) {
                if (protocol.indexOf('https') !== -1) {
                    return 'https';
                } else {
                    return 'http';
                }
            }
        }
        //-->
    </script>
    <!-- fin script publicidad -->



    <!-- Script ad server broadstreet -->
    <script src="http://cdn.broadstreetads.com/init.js"></script>
    <!-- Script ad server broadstreet -->

    <!-- Meta por cartel de facebook -->
    <meta propiedad="fb: páginas" contenido="251699338005"/>
    <!-- Meta por cartel de facebook -->


</head>
<body class="<?php print $classes; ?>" <?php print $attributes; ?>>


<!-- Google Tag Manager gallery-->

<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5B5J4F"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5B5J4F');</script>


<!-- End Google Tag Manager gallery-->



<!-- Google Tag Manager cartel comscore-->


<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-57SFLT"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-57SFLT');</script>


<!-- End Google Tag Manager cartel comscore -->


<div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
</div>
<?php print $page_top; ?>
<?php print $page; ?>
<?php print $page_bottom; ?>
</body>
</html>