<?php
/**
 * @file
 * Default theme implementation to format an HTML mail.
 *
 * Copy this file in your default theme folder to create a custom themed mail.
 * Rename it to mimemail-message--[module]--[key].tpl.php to override it for a
 * specific mail.
 *
 * Available variables:
 * - $recipient: The recipient of the message
 * - $subject: The message subject
 * - $body: The message body
 * - $css: Internal style sheets
 * - $module: The sending module
 * - $key: The message identifier
 *
 * @see template_preprocess_mimemail_message()
 */
global $base_url;
?>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="<?php print $base_url . "/" . path_to_theme() . "/assets/css/mail.css" ?>"> 
  </head>

  <body  id="mimemail-body" <?php if ($module && $key): print 'class="' . $module . '-' . $key . '"';
endif; ?>>

    <table class="container-mail" style="margin: auto; padding-top: 20px; padding-bottom: 20px; color : #333333; line-height:18px; font-family: 'Open Sans', sans-serif;">
      <tr>
        <td>
          <table class="header" style="width: 100%; padding: 10px 20px 0px; background: black;">
            <tr>
              <td>
                <a style="border:none; outline:none" href="<?php print url('', array('absolute' => true)) ?>">
                  <img width="360px" height="62px" style="display:block;" src="<?php print $base_url . "/" . path_to_theme() . "/logo-cartel-urbano.png" ?>" />
                </a>
              </td>
            </tr>
          </table>
        </td>
      </tr>

      <tr class="body-mail">
        <td>
          <table class="center"  style="width: 80%; margin: 20px 11%; border: 1px solid #6f6f6f;">      
            <tr>
              <td style="padding-top: 50px;">
                <h2 style="margin-left: 20px;">Bienvenido a Cartel Urbano</h2>
              </td>
            </tr>
            <tr>
              <td style="padding: 10px 20px;">
                <?php print $body ?>
              </td>
            </tr>
          </table>
        </td>
      </tr>

      <tr class="footer" style="text-align: center; background: black;">
        <td>
          <p class="terms-conditions" style="padding: 0 100px; font-size: 15px; color: white;">
            Carrera 13a No. 37 - 01 / Tel (571) 288 3779. Bogotá, Colombia
          </p>    
          <p class="terms-conditions" style="padding: 0 100px; font-size: 15px; color: white;">
            Todos los derechos reservados, Cartel Media S.A.S © 2015
          </p>   
        </td>
      </tr>

    </table>
</html>
