<?php

/**
 * @file
 * Implementation to display a single Drupal page while offline.
 *
 * All the available variables are mirrored in page.tpl.php.
 *
 * @see template_preprocess()
 * @see template_preprocess_maintenance_page()
 * @see bartik_process_maintenance_page()
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
<head>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>

  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
  </div>

  <div id="page-wrapper"><div id="page">


    <div id="main-wrapper"><div id="main" class="clearfix">
      <div id="content" class="column"><div class="section">
        
<div class="text-wrapper        ">
<div class="title " data-content="¡¡rayos!! Sentimos un disturbio en la fuerza">¡¡rayos!! Sentimos un disturbio en la fuerza ...</div>

<div class="subtitle" data-content="Ya regresamos">Ya regresamos</div>


</div>
<style type="text/css">body {
  background: url(https://31.media.tumblr.com/2ccdc6d26066a61bfaa3fd2c04333c99/tumblr_noximcr8bF1slaspwo1_500.gif) no-repeat center center fixed;
  background-size: cover;
  height: 100%;
}

.text-wrapper {
  height: 100%;
font-family: 'bebas_neueregular',Arial-narrow;
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
  -webkit-flex-direction: column;
      -ms-flex-direction: column;
          flex-direction: column;
  -webkit-box-align: center;
  -webkit-align-items: center;
      -ms-flex-align: center;
          align-items: center;
  -webkit-box-pack: center;
  -webkit-justify-content: center;
      -ms-flex-pack: center;
          justify-content: center;
}

.title {
  font-size: 40px;
  font-weight: 700;
  color: white;
  position: relative;
  margin-top: 100px;

}

.subtitle {
  font-size: 30px;
  font-weight: 700;
  color: white;
  position: relative;
  line-height: 150px;

}

.buttons {
  margin: 30px;
}
.buttons a.button {
  background-color: #07b9e3;
  font-weight: 700;
  border: 2px solid white;
  text-decoration: none;
  padding: 15px;
  text-transform: uppercase;
  color: white;
  -webkit-transition: all 0.2s ease-in-out;
          transition: all 0.2s ease-in-out;
}
.buttons a.button:hover {
  background-color: #000;
  color: white;
  -webkit-transition: all 0.2s ease-in-out;
          transition: all 0.2s ease-in-out;
}

@-webkit-keyframes noise-anim {
  0% {
    clip: rect(134px, 9999px, 104px, 0);
  }
  5% {
    clip: rect(34px, 9999px, 8px, 0);
  }
  10% {
    clip: rect(57px, 9999px, 154px, 0);
  }
  15.0% {
    clip: rect(21px, 9999px, 33px, 0);
  }
  20% {
    clip: rect(140px, 9999px, 112px, 0);
  }
  25% {
    clip: rect(94px, 9999px, 189px, 0);
  }
  30.0% {
    clip: rect(135px, 9999px, 100px, 0);
  }
  35% {
    clip: rect(164px, 9999px, 90px, 0);
  }
  40% {
    clip: rect(60px, 9999px, 67px, 0);
  }
  45% {
    clip: rect(76px, 9999px, 66px, 0);
  }
  50% {
    clip: rect(39px, 9999px, 83px, 0);
  }
  55.0% {
    clip: rect(135px, 9999px, 135px, 0);
  }
  60.0% {
    clip: rect(60px, 9999px, 136px, 0);
  }
  65% {
    clip: rect(4px, 9999px, 143px, 0);
  }
  70% {
    clip: rect(1px, 9999px, 26px, 0);
  }
  75% {
    clip: rect(128px, 9999px, 179px, 0);
  }
  80% {
    clip: rect(184px, 9999px, 107px, 0);
  }
  85.0% {
    clip: rect(75px, 9999px, 130px, 0);
  }
  90% {
    clip: rect(42px, 9999px, 161px, 0);
  }
  95% {
    clip: rect(34px, 9999px, 150px, 0);
  }
  100% {
    clip: rect(136px, 9999px, 5px, 0);
  }
}

@keyframes noise-anim {
  0% {
    clip: rect(134px, 9999px, 104px, 0);
  }
  5% {
    clip: rect(34px, 9999px, 8px, 0);
  }
  10% {
    clip: rect(57px, 9999px, 154px, 0);
  }
  15.0% {
    clip: rect(21px, 9999px, 33px, 0);
  }
  20% {
    clip: rect(140px, 9999px, 112px, 0);
  }
  25% {
    clip: rect(94px, 9999px, 189px, 0);
  }
  30.0% {
    clip: rect(135px, 9999px, 100px, 0);
  }
  35% {
    clip: rect(164px, 9999px, 90px, 0);
  }
  40% {
    clip: rect(60px, 9999px, 67px, 0);
  }
  45% {
    clip: rect(76px, 9999px, 66px, 0);
  }
  50% {
    clip: rect(39px, 9999px, 83px, 0);
  }
  55.0% {
    clip: rect(135px, 9999px, 135px, 0);
  }
  60.0% {
    clip: rect(60px, 9999px, 136px, 0);
  }
  65% {
    clip: rect(4px, 9999px, 143px, 0);
  }
  70% {
    clip: rect(1px, 9999px, 26px, 0);
  }
  75% {
    clip: rect(128px, 9999px, 179px, 0);
  }
  80% {
    clip: rect(184px, 9999px, 107px, 0);
  }
  85.0% {
    clip: rect(75px, 9999px, 130px, 0);
  }
  90% {
    clip: rect(42px, 9999px, 161px, 0);
  }
  95% {
    clip: rect(34px, 9999px, 150px, 0);
  }
  100% {
    clip: rect(136px, 9999px, 5px, 0);
  }
}
.subtitle:after, .title:after {
  content: attr(data-content);
  position: absolute;
  left: 2px;
  text-shadow: -1px 0 red;
  top: 0;
  color: white;
  overflow: hidden;
  clip: rect(0, 900px, 0, 0);
  -webkit-animation: noise-anim 2s infinite linear alternate-reverse;
          animation: noise-anim 2s infinite linear alternate-reverse;
}

@-webkit-keyframes noise-anim-2 {
  0% {
    clip: rect(101px, 9999px, 47px, 0);
  }
  5% {
    clip: rect(16px, 9999px, 183px, 0);
  }
  10% {
    clip: rect(101px, 9999px, 7px, 0);
  }
  15.0% {
    clip: rect(62px, 9999px, 31px, 0);
  }
  20% {
    clip: rect(62px, 9999px, 154px, 0);
  }
  25% {
    clip: rect(160px, 9999px, 14px, 0);
  }
  30.0% {
    clip: rect(76px, 9999px, 57px, 0);
  }
  35% {
    clip: rect(17px, 9999px, 21px, 0);
  }
  40% {
    clip: rect(76px, 9999px, 146px, 0);
  }
  45% {
    clip: rect(94px, 9999px, 187px, 0);
  }
  50% {
    clip: rect(50px, 9999px, 158px, 0);
  }
  55.0% {
    clip: rect(156px, 9999px, 41px, 0);
  }
  60.0% {
    clip: rect(169px, 9999px, 35px, 0);
  }
  65% {
    clip: rect(32px, 9999px, 140px, 0);
  }
  70% {
    clip: rect(182px, 9999px, 106px, 0);
  }
  75% {
    clip: rect(46px, 9999px, 116px, 0);
  }
  80% {
    clip: rect(52px, 9999px, 132px, 0);
  }
  85.0% {
    clip: rect(2px, 9999px, 75px, 0);
  }
  90% {
    clip: rect(92px, 9999px, 187px, 0);
  }
  95% {
    clip: rect(99px, 9999px, 9px, 0);
  }
  100% {
    clip: rect(68px, 9999px, 193px, 0);
  }
}

@keyframes noise-anim-2 {
  0% {
    clip: rect(101px, 9999px, 47px, 0);
  }
  5% {
    clip: rect(16px, 9999px, 183px, 0);
  }
  10% {
    clip: rect(101px, 9999px, 7px, 0);
  }
  15.0% {
    clip: rect(62px, 9999px, 31px, 0);
  }
  20% {
    clip: rect(62px, 9999px, 154px, 0);
  }
  25% {
    clip: rect(160px, 9999px, 14px, 0);
  }
  30.0% {
    clip: rect(76px, 9999px, 57px, 0);
  }
  35% {
    clip: rect(17px, 9999px, 21px, 0);
  }
  40% {
    clip: rect(76px, 9999px, 146px, 0);
  }
  45% {
    clip: rect(94px, 9999px, 187px, 0);
  }
  50% {
    clip: rect(50px, 9999px, 158px, 0);
  }
  55.0% {
    clip: rect(156px, 9999px, 41px, 0);
  }
  60.0% {
    clip: rect(169px, 9999px, 35px, 0);
  }
  65% {
    clip: rect(32px, 9999px, 140px, 0);
  }
  70% {
    clip: rect(182px, 9999px, 106px, 0);
  }
  75% {
    clip: rect(46px, 9999px, 116px, 0);
  }
  80% {
    clip: rect(52px, 9999px, 132px, 0);
  }
  85.0% {
    clip: rect(2px, 9999px, 75px, 0);
  }
  90% {
    clip: rect(92px, 9999px, 187px, 0);
  }
  95% {
    clip: rect(99px, 9999px, 9px, 0);
  }
  100% {
    clip: rect(68px, 9999px, 193px, 0);
  }
}
.subtitle:before, .title:before {
  content: attr(data-content);
  position: absolute;
  left: -2px;
  text-shadow: 1px 0 blue;
  top: 0;
  color: white;
  overflow: hidden;
  clip: rect(0, 900px, 0, 0);
  -webkit-animation: noise-anim-2 3s infinite linear alternate-reverse;
          animation: noise-anim-2 3s infinite linear alternate-reverse;
}
</style>

        </div> <!-- /.section, /#content -->
    </div></div> <!-- /#main, /#main-wrapper -->

  </div></div> <!-- /#page, /#page-wrapper -->

</body>
</html>
