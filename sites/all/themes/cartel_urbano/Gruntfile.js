'use strict';
module.exports = function(grunt) {
  // cargamos todas las tareas
  require('load-grunt-tasks')(grunt);

  var jsFileList = [
    'assets/vendor/bootstrap/js/transition.js',
    'assets/vendor/bootstrap/js/alert.js',
    'assets/vendor/bootstrap/js/button.js',
    'assets/vendor/bootstrap/js/carousel.js',
    'assets/vendor/bootstrap/js/collapse.js',
    'assets/vendor/bootstrap/js/dropdown.js',
    'assets/vendor/bootstrap/js/modal.js',
    'assets/vendor/bootstrap/js/tooltip.js',
    'assets/vendor/bootstrap/js/popover.js',
    'assets/vendor/bootstrap/js/scrollspy.js',
    'assets/vendor/bootstrap/js/tab.js',
    'assets/vendor/bootstrap/js/affix.js',
    'assets/vendor/scroll/perfect-scrollbar.js',
    'assets/vendor/smoothState/jquery.smoothState.js',
    'assets/js/slick.min.js',
    'assets/js/jquery.galleriffic.js',
    'assets/js/jquery.opacityrollover.js',
   // 'assets/js/jquery.mobile-1.4.5.js',
    'assets/js/_*.js'
  ];

  // Configuracion del proyecto.
  grunt.initConfig({
    less: {
      dev: {
        files: {
          "assets/css/main.css": [
            "assets/less/master.less"
          ]
        },
        options: {
          compress: true,
          //source maps
          strictMath: true,
          sourceMap: true,
          outputSourceFiles: true,
          sourceMapURL: 'main.css.map',
          sourceMapFilename: 'assets/css/main.css.map'
        }
      },
      build: {
        files: {
          "assets/css/main.css": [
            "assets/less/master.less"
          ]
        },
        options: {
          compress: true
        }
      }
    },
    concat: {
      options: {
        separator: ';',
      },
      dist: {
        src: [jsFileList],
        dest: 'assets/js/scripts.js',
      }
    },
    uglify: {
      dist: {
        files: {
          'assets/js/scripts.min.js': [jsFileList]
        }
      }
    },
    autoprefixer: {
      options: {
        browsers: ['last 2 versions', 'ie 8', 'ie 9', 'android 2.3', 'android 4', 'opera 12']
      },
      dev: {
        options: {
          map: {
            prev: 'assets/css/'
          }
        },
        src: 'assets/css/main.css'
      },
      build: {
        src: 'assets/css/main.css'
      }
    },
    modernizr: {
      build: {
        devFile: 'assets/vendor/modernizr/modernizr.js',
        outputFile: 'assets/js/vendor/modernizr.min.js',
        files: {
          'src': [
            ['assets/js/scripts.min.js'],
            ['assets/css/main.min.css']
          ]
        },
        uglify: true,
        parseFiles: true
      }
    },
    watch: {
      less: {
        files: [
          'assets/less/*.less',
          'assets/less/*/*.less'
        ],
        tasks: ['less:dev', 'autoprefixer:dev']
      },
      js: {
        files: [
          jsFileList
        ],
        tasks: ['concat']
      },
      livereload: {
        // Browser live reloading
        // https://github.com/gruntjs/grunt-contrib-watch#live-reloading
        options: {
          livereload: false
        },
        files: [
          'assets/css/main.css',
          'assets/js/scripts.js'
        ]
      }
    }
  });

  // resgistrar las tareas
  grunt.registerTask('default', [
    'dev'
  ]);

  grunt.registerTask('dev', [
    'less:dev',
    'autoprefixer:dev',
    'concat'
  ]);

  grunt.registerTask('build', [
    'less:build',
    'autoprefixer:build',
    'uglify'
  ]);
};