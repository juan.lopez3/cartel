(function($, window, undefined) {

	//condicion solo para desktop
	if ( $( window ).width() <= 640 ){ return; }

	$(document).ready(function() {
		$('section#block-webform-client-block-602, section#block-webform-client-block-601, .pane-cu-mas-popular-block-mas-popula').perfectScrollbar({
			suppressScrollX: true
		});

		//layout split selector
		var splitLayout = $('.layout-split');
		
		splitLayout.find('.region-left, .region-right').height($(window).height() - 200);
		//perfect scroll contianer - SPLIT LAYOUT
		splitLayout.find('.region-left, .region-right').perfectScrollbar({ suppressScrollX: true });
	});

	//CUSTOM SCROLL
	$(window).load(function() {
		//contendeor lo más popular - home
		var loMasPopular = $('.pane-relacionado, .lomaspopular');

  		//LO MAS POPULAR - AJUSTA ALTURA DEL CONTENEDOR
		$(window).resize(function() {
			$('.region-destacado .pane-relacionado .pane-content').height($('.region-destacado .pane-relacionado').height() - 100);
			$('.region-top .lomaspopular .pane-content').height($('.region-top .lomaspopular').height() - 100);
			//$('.layout-split').find('.region-left, .region-right').height($(window).height() - 200);
			$('.layout-split').find('.region-left, .region-right').height($(window).height() - 140);
		});

		$(window).trigger('resize');

		//Perfect scroll container - lo + popular
		loMasPopular.find('.pane-content').perfectScrollbar({
			suppressScrollX: true
		});

		//lo más popular - crea boton para explandir y contraer (expandido por defecto)
		//////////////////////////////////////////////////////////////////////////////
		loMasPopular.append('<div class="togglePopularBar"></div>');

		//lo más popular - logic!
		loMasPopular.on('click', '.togglePopularBar', function() {
			loMasPopular.toggleClass('activated');
		});

		loMasPopular.delay(3000).queue(function(next){
            loMasPopular.toggleClass('activated');
            next();
        });
	});

})(jQuery, window);
