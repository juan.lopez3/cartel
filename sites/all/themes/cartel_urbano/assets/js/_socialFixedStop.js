(function($, window, undefined) {

    $(document).ready(function() {
        var menu = $('.group-redes-sociales');
        var contenedor = $('.region-left').parents('.region.row');
        var cont_offset = contenedor.height();

        $(window).on('scroll', function() {
            console.log(cont_offset);
            if($(window).scrollTop() > cont_offset) {
                menu.addClass('social-fixed');
            } else {
                menu.removeClass('social-fixed');
            }
        });
    });


})(jQuery, window);
