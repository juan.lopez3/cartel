(function($, window, undefined) {

	//Extras - social share, imagen landing parallax
	$(document).ready(function() {
		//muestra y oculta redes
		$('.pane-cu-list-content-section-block-list-content-section,  .node-video, .view-nodequeue-1, .node-gallery, .node-eventos').on('click', 'span.count_total', function() {
			$(this).closest('.field-name-social-network').toggleClass('active');
		});


		//DFP quita la clase del div de pauta para mostrarlo
		$('#dfp-ad-page_out_popup-wrapper').removeClass('element-hidden');


		//parallax img landing
		$(window).bind('scroll', function(e) {
			var val = $(this).scrollTop();
			parallax(val);
		});

		function parallax(scrollTop) {
			//cabezote de seccion
			var elm = $('.one-column-top').find('.cabezote-seccion');

			elm.find('img').css({
				transform: 'translate3d(0,' + (scrollTop - 60) / 2 + 'px,0)'
			});
		}

		//fondos administrables
		$('.region-publicidad').find('.publicidad').each(function() {
			var fondoPauta = $(this).data('bg-pub');

			$(this).closest('.region-publicidad').css({
				backgroundImage : 'url(' + fondoPauta + ')'
			});
		});

		// fondo opinion home --------------->
		var fondoOpinion = $('.theme-block-opinion').data('uribg');
		$('.home-panel').find('.opinion').css({
			backgroundImage : 'url(' + fondoOpinion + ')'
		});

		//fondo publicidad cuadrada home ------------------>
		$('.publicidad').each(function() {
			var fondoPauta = $(this).data('bg-pub');

			$(this).css({
				backgroundImage : 'url(' + fondoPauta + ')'
			});
		});

		//quitar panel separator de publicidad, rompe el float!
		$('.region-main').find('.pane-cartel-section-publicidad').prev('.panel-separator').remove();


	
		/*
		 ocultando pauta si existe imagen
		*/
		// banner superior
		if ($('.field.field-name-field-imagen-728-arriba.field-type-image.field-label-hidden').length ) {
			$('.field.field-name-publicidad-2.field-type-ds.field-label-hidden').hide();
		}

		// banner lateral superior
		if ($('.field.field-name-field-imagen-banner-arriba-.field-type-image.field-label-hidden').length ) {
			$('.field.field-name-publicidad-right-banner-2.field-type-ds.field-label-hidden').hide();
		}
		// banner lateral inferior
		if ($('.field.field-name-field-imagen-banner-abajo.field-type-image.field-label-hidden').length ) {
			$('.field.field-name-publicidad.field-type-ds.field-label-hidden').hide();
		}

	});



})(jQuery, window);