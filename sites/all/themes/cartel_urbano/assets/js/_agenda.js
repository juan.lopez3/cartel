(function($, window, undefined) {
	$(document).ready(function() {
		//fix videos interna evento
		var videoBody = $('.node-type-eventos').find('.field-name-body').find('p iframe');
		videoBody.closest('p').addClass('media_embed');

		//convertimos los dropdowns en listas
		$('.agenda-eventos').find('.form-select').select2();

	});

	Drupal.behaviors.modificar_select_tags = {
		attach: function(context, settings) {
		  $(document).ajaxComplete(function(event, xhr, settings) {
		    //event.stopImmediatePropagation();
		    //event.stopPropagation();
		    $('.agenda-eventos').find('.form-select').select2();
		  });
		}
	};


})(jQuery, window);
