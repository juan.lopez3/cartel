(function($, window, undefined) {

	$(document).ready(function() {

		var uriImgNodo;
		//contenedor principal de sección
		var ContainerSection = $('.container_section');
		//revisamos si el contenido es split
		var checkSplit = $('body').find('.layout-split');

		if ( checkSplit.length ) {
			$('body').addClass('split-node');
		}

		//establecer imagen para cada seccion
		ContainerSection.each( function() {
			//capturamos la url de la imagen de seccion
			var getSectionImage = $(this).find('.taxonomy-term').find('img').attr('src');
			//ponemos la imagen como fondo
			//$(this).css({ backgroundImage: 'url(' + getSectionImage + ')' });
		});

		//PONER IMAGEN DEL NODO COMO FONDO DE SECCION
		ContainerSection.on('mouseenter', '.node', function() { 
			uriImgNodo = $(this).attr('datauri');
	 		$(this).closest('.container_section').css({
	 			backgroundImage: 'url(' + uriImgNodo + ')'
	 		});
		});

		//OPINION TOGGLE CLASE ACTIVA
		$('.container_nodes').on('click', '.views-list-columns', function() {
			ocultarContenedorDos();
			$(this).parent().next(contenedorDos).addClass('active');
		});

		$('.views-last-column').on('click', function() {
			ocultarContenedorDos();
		});
	});
	
	//OPINION oculta contenedor
	var contenedorDos = '#contenedor_dos, #contenedor_dos_clone';
	function ocultarContenedorDos() { $(contenedorDos).removeClass('active'); }

})(jQuery, window);