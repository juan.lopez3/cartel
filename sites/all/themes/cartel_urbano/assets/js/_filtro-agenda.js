(function($, window, undefined) {
  $(document).ready(function() {
    Drupal.behaviors.events = {
      attach: function(context, settings) {
        $(document).ajaxComplete(function(event, xhr, settings) {
          event.stopImmediatePropagation();
        });
      }
    };

    $(document).bind("ajaxStart", function() {
      //cuando esta cargando los nodos
      $('.agenda-eventos').removeClass('eventos-cargados').addClass('cargando-eventos');
       console.log( "test" );
    }).bind("ajaxComplete", function() {
      //cuando carga los nodos
      $('.agenda-eventos').removeClass('cargando-eventos').addClass('eventos-cargados');
    });

  });//document ready

})(jQuery, window);
