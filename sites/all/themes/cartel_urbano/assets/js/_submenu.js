(function($, window, undefined) {

	//variables
	var secciones = {
		1 : 'musica',
		2 : 'deportes',
		3 : 'arte',
		4 : 'noticias',
		5 : 'sexo',
		6 : 'opinion',
		7 : 'diseno',
		9 : 'historias',
		10: 'moda'
	};

	$(document).ready(function() {

		$('#menu-sections').on('mouseenter', '> li', function (e) {
			var seccionId = $(this).attr('id');

			//limpiamos las clases en el selector
			$('#div_sub_menu_section').attr('class', '');

			//verificamos que el valor en el array no aroje un valor vacio
			if ( secciones[seccionId] == null ) { return; }

			//agregamos la calse dependiendo de la seccion
			$('#div_sub_menu_section').addClass(secciones[seccionId]);
		});

	});

})(jQuery, window);