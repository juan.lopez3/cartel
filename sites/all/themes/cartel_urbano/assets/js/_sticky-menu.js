(function($, window, undefined) {
	$(document).ready(function() {

		//var stickyNavTop = $('#block-cu-menu-sections-block-menu-sections').offset().top;
       var stickyNavTop = $('#block-cu-menu-sections-block-menu-links-extras').offset().top;
		var stickyNav = function(){  
			var scrollTop = $(window).scrollTop();  
			       
			if (scrollTop > stickyNavTop) {   
			    $('.region-header-seccion, body').addClass('sticky');
			  //  $('.main-logo-home').addClass('sticky');
			    $('.abrir-menu').addClass('sticky');
			} else {  
			    $('.region-header-seccion, body').removeClass('sticky');
              //  $('.main-logo-home').removeClass('sticky'); 
                $('.abrir-menu').removeClass('sticky');
			}
		};
		  



		stickyNav();
		  
		$(window).scroll(function() {  
		    stickyNav();
		});



	});

})(jQuery, window);
