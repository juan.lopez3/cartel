(function($, window, undefined) {

	$(document).on('ready', function(){
		var _wrapDestacado = $('.pane-nodequeue-1'),
			_imgDestacado = _wrapDestacado.find('.field-name-field-imagen-cuadrada').find('img');

		if (_imgDestacado.length > 0) {
			var urlImgDestacado = _imgDestacado.attr('src');

			_wrapDestacado.css({
				backgroundImage: 'url('+ urlImgDestacado +')'
			});

			console.log(urlImgDestacado);
		}
	});

})(jQuery, window);