// external js: masonry.pkgd.js, imagesloaded.pkgd.js
(function($, window, undefined) {
   $(document).ready( function() {

      // init Masonry
      var $grid = $('.view-id-nodequeue_5 ul').masonry({
         itemSelector: '.views-row',
         percentPosition: true,
         columnWidth: '.views-row'
      });
      // layout Isotope after each image loads
      $grid.imagesLoaded().progress( function() {
         $grid.masonry();
      });

   });


})(jQuery, window);
