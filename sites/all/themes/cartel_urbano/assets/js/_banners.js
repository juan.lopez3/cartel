(function () {

	jQuery(document).ready(function(){

		// Pop-up
		// Cambio
		if (jQuery('.region-banner-pop-up').is(":visible")){

			jQuery("body").addClass("noscroll");

			jQuery(".region-banner-pop-up section").prepend("<p id='wait'>Espera <span id='count'>5</span> segundos</p>");
			jQuery(".region-banner-pop-up section").prepend("<img class='close-popup' width='25' height='25' src='/sites/all/themes/cartel_urbano/assets/img/times-circle-o.png' alt='Cerrar Pop-up' />");

			(function(){
			  var counter = 5;

			  setInterval(function() {
			    counter--;
			    if (counter >= 0) {
			      span = document.getElementById("count");
			      span.innerHTML = counter;
			    }
			    // Display 'counter' wherever you want to display it.
			    if (counter === 0) {
			    	jQuery(".region-banner-pop-up #wait").hide();
			        jQuery(".region-banner-pop-up .close-popup").show();
			        clearInterval(counter);
			    }
			    
			  }, 1000);
			    
			})();

		    jQuery(".region-banner-pop-up .close-popup").click(function(){
				jQuery(".region-banner-pop-up").fadeOut("slow", function(){
					jQuery("body").removeClass("noscroll");
				});
			});

		}

		// Welcome mat

		// if (jQuery('.region-banner-welcome-mat').is(":visible")){

		// 	jQuery("body").addClass("noscroll");

		// 	jQuery(".region-banner-welcome-mat").append("<img class='close-mat' width='38' height='38' src='/sites/all/themes/cartel_urbano/assets/img/arrow-circle-down.png' alt='Cerrar banner' />");

		// 	jQuery(".region-banner-welcome-mat .close-mat").click(function(){
		// 		jQuery(".region-banner-welcome-mat").slideToggle("slow", function(){
		// 			jQuery("body").removeClass("noscroll");
		// 		});
		// 	});
		// }

		// Sticky bar desplegable

		jQuery(".region-banner-sticky-bar-desplegable section").prepend("<img class='close-sticky' width='25' height='25' src='/sites/all/themes/cartel_urbano/assets/img/times-circle-o-blue.png' alt='Cerrar banner' />");
		// jQuery('.region-banner-sticky-bar-desplegable').show('slow');
		jQuery('.region-banner-sticky-bar-desplegable .close-sticky').click(function(){
			jQuery('.region-banner-sticky-bar-desplegable').hide('slow');
		});

		
			jQuery(".region-banner-sticky-bar-desplegable").animate({
				bottom: 0
			}, 2000, function(){
				// Animation complete
			});
		
	})

	// Header banner

	// var video = '<iframe width="498" height="280" src="https://www.youtube.com/embed/NL-KcBp-lfw?showinfo=0" frameborder="0" allowfullscreen></iframe>';

	// jQuery(".region-banner-header p").html(video);
	// jQuery(".region-banner-header").show('slow');

}());

