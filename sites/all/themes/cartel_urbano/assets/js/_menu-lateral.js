(function($, window, undefined) {


   $(document).ready(function() {

      if (screen.width<768) {
         //$('#block-system-main-menu').toggleClass('inactive');
         $('.logo_site').toggleClass('full-logo');
      }


      var _mainSelector = $('#block-system-main-menu');
      //ABRIR MENU LATERAL
      $('.abrir-menu, .logo-sidebar').on('click', function(e) {


         _mainSelector.toggleClass('inactive');
         $('.main-container > .row').toggleClass('full');
         //$('body').toggleClass('MenuLateralActivo');
         $('.footer').toggleClass('full');
         //$('body').toggleClass('MenuLateralActivo');
         $('.logo_site').toggleClass('full-logo');
         //$('body').toggleClass('MenuLateralActivo');

         $('body').on("click",function(){

            _mainSelector.removeClass('active');
            $('body').removeClass('MenuLateralActivo');

         });

         setTimeout(ajusteNodos,100);
         e.stopImmediatePropagation();



      });

      //agregar boton cerrar submenu
      _mainSelector.find('.menu-attach-block-wrapper').find('section').prepend('<div class="cerrar-submenu">x</div>');

      $('.cerrar-submenu').on('click', function () {
         //
         _mainSelector.removeClass('active');
         $('body').removeClass('MenuLateralActivo');
         $('body').off("click");

         setTimeout(ajusteNodos,100);
         setTimeout(ajusteNodosInternos,100);

      });

      _mainSelector.on('click', 'li.leaf > a', function() {
         var _itemPadre = $(this).parent();
         _mainSelector.find('li.leaf').removeClass('active');
         _mainSelector.find('.menu-attach-block-wrapper').find('section').removeClass('active');
         _itemPadre.find('section').addClass('active');
         _itemPadre.addClass('active');
         _mainSelector.addClass('activeSubMenu');
      });

      /*masonry nodos home*/
      function ajusteNodos(){
         // init Masonry
         var $grid = $('.view-id-nodequeue_5').masonry({
            itemSelector: '.views-row',
            percentPosition: true,
            columnWidth: '.views-row'
         });
         // layout Isotope after each image loads
         $grid.imagesLoaded().progress( function() {
            $grid.masonry();
         });

      }
      /*masonry nodos pages*/
      function ajusteNodosInternos(){
         // init Masonry
         var $grid = $('.pane-block').masonry({
            itemSelector: '.ds-1col',
            percentPosition: true,
            columnWidth: '.views-row'
         });
         // layout Isotope after each image loads
         $grid.imagesLoaded().progress( function() {
            $grid.masonry();
         });

      }

      
     // $(".abrir-menu").click();
      //$(".row").addClass("full");
      //$("#block-system-main-menu").addClass("inactive");
      //$(".footer.container-fluid").addClass("full");

   });
})(jQuery, window);
