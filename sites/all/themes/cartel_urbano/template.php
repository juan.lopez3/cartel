<?php


/**
 * @file
 * template.php
 */
drupal_get_library('Mobile_Detect', 'Mobile_Detect');

//para reducir las lineas de código en la plantilla  
global $p;
$url_base = '';
$p = url("", array("absolute" => true)) . $url_base . "";

global $t;
$t = base_path() . path_to_theme() . "";

function cartel_urbano_item_list($variables) {
  $items = $variables['items'];
  $title = $variables['title'];
  $type = $variables['type'];
  $attributes = $variables['attributes'];

  $wrapper_attributes = $variables['wrapper_attributes'];
  if (!isset(
                  $wrapper_attributes['class'])) {
    $wrapper_attributes['class'] = array('item-list');
  }

// Use drupal attributes function to convert an array into HTML attributes
  $output = '<div' . drupal_attributes($wrapper_attributes) . '>';
  if (isset($title) && $title !== '') {
    $output .= '<h3>' . $title . '</h3>';
  }
  if (!empty(
                  $items)) {
    $output .= "<$type" . drupal_attributes($attributes) . '>';
    $num_items = count($items);
    $i = 0;
    foreach ($items as $item) {
      $attributes = array();
      $children = array();
      $data = '';
      $i++;
      if (is_array($item)) {
        foreach ($item as $key => $value) {
          if ($key == 'data') {
            $data = $value;
          } elseif ($key == 'children') {
            $children = $value;
          } else {
            $attributes[$key] = $value;
          }
        }
      } else {
        $data = $item;
      }
      if (count($children) > 0) {
        // Render nested list.
        $data .= theme_item_list(array('items' => $children, 'title' => NULL, 'type' => $type, 'attributes' => $attributes));
      }
      if ($i == 1) {
        $attributes['class'][] = 'first';
      }
      if ($i == $num_items) {
        $attributes['class'][] = 'last';
      }
      $output .= '<li' . drupal_attributes($attributes) . '>' . $data . "</li>\n";
    }
    $output .= "</$type>";
  }
  $output .= '</div>';
  return $output;
}

function cartel_preprocess_html(&$variables) {

  $detect = mobile_detect_get_object();
  $is_mobile = $detect->isMobile();

  $variables['classes_array'][] = ($is_mobile) ? 'mobile-site': 'desktop-site';

  $nid = explode('/', current_path());

  if (isset($variables['page']['content']['system_main']['nodes'])) {

    $title = null;
    if(isset($nid[1])){

      $title = isset($variables['page']['content']['system_main']['nodes'][$nid[1]]['field_seccion'][0]['#title']) ? $variables['page']['content']['system_main']['nodes'][$nid[1]]['field_seccion'][0]['#title'] : null;
    }

    if ($title != null) {
      $name_term_node = $variables['page']['content']['system_main']['nodes'][$nid[1]]['field_seccion'][0]['#title'];
      $name_term_node = str_replace(array(" "), '_', $name_term_node);
      $variables['classes_array'][] = 'taxonomy-term-' . strtolower($name_term_node);
    }
  }

  $explode = explode('/', $_GET['q']);
  if ($explode[0] == 'node') {
    if(isset($explode[1])){
      $node = node_load($explode[1]);
      if (isset($node->field_seccion['und'][0])) {
        $tid = $node->field_seccion['und'][0]['tid'];
      }
    }
  }

  if ($explode[0] == 'taxonomy') {
    if (isset($explode[2])) {
      $tid = $explode[2];
    }
  }
  if (isset($tid)) {
    $term = taxonomy_term_load($tid);
    if (isset($term->field_script_ads['und'][0])) {
      $script = $term->field_script_ads['und'][0]['value'];
      $variables['data_script_ads'] = $script;
    }
  }
}

function cartel_preprocess_node(&$variables) {
  if (isset($variables['field_imagen'])) {
    if (isset($variables['field_imagen']['und'][0]['uri'])) {
      $x = $variables['field_imagen']['und'][0]['uri'];
      $path_real_file = drupal_realpath($x);
    } else if (isset($variables['field_imagen'][0]['uri'])) {
      $x = $variables['field_imagen'][0]['uri'];
      $path_real_file = drupal_realpath($x);
    }
  }
  if (isset($variables['field_imagenes'])) {
    if (isset($variables['field_imagenes']['und'][0]['uri'])) {
      $x = $variables['field_imagenes']['und'][0]['uri'];
      $path_real_file = drupal_realpath($x);
    }
    if (isset($variables['field_imagenes'][0]['uri'])) {
      $x = $variables['field_imagenes'][0]['uri'];
      $path_real_file = drupal_realpath($x);
    }
  }

  if (isset($path_real_file)) {
    $e = explode('sites', $path_real_file);
    $f = isset($e[1]) ? $e[1] : null;
    if ($f != null) {
      $d = 'sites' . $f;
      global $p;
      $variables['data_uri'] = isset($d) ? $p . $d : null;
    }
  }
  if (isset($variables['field_seccion'][0]['tid'])) {
    $name_term_node = isset($variables['field_seccion'][0]['taxonomy_term']->name) ? $variables['field_seccion'][0]['taxonomy_term']->name : null;
    if ($name_term_node != null) {
      $name_term_node = str_replace(array(" "), '_', $name_term_node);
      $variables['classes_array'][] = 'taxonomy-term-' . strtolower($name_term_node);
    }
  }

  if ($variables['type'] == 'video' && $variables['is_front'] == TRUE) {
    $variables['id_node'] = 'node-' . $variables['nid'];
  }
}

function cartel_preprocess_taxonomy_term(&$variables) {
  //agregarndo clase en el body para el landing de seccion y para un nodo que pertenesca a una seccion
  $name_term_node = $variables['name'];
  $name_term_node = str_replace(array(" "), '_', $name_term_node);
  $variables['classes_array'][] = 'taxonomy-term-' . strtolower($name_term_node);
}

function cartel_menu_link(array $variables){
  if ($variables['element']['#theme'] == 'menu_link__main_menu') {
    if ($variables['element']['#localized_options']['menu_attach_block']['name'] == 'block|17') {
      $variables['element']['#attributes']['class'][] = 'edicion-impresa';
    }
    if ($variables['element']['#href'] == 'proteccion-de-datos') {
     $variables['element']['#attributes']['class'][] = 'proteccion-datos'; 
    }
}
  return theme_menu_link($variables);
}